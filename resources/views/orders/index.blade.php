@extends('layouts.fixed')
@section('title', 'Orders')
@section('breadcrumb')
    <li class="breadcrumb-item active">Orders</li>
@endsection
@section('content')
    <section class="content mt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary col-md-12">
                        <div class="card-header row">
                            <h3 class="card-title col-md-3">All Orders <a title="Click to add new product" class="btn btn-sm " href="{{ route('orders.create')}}">
                                <span class="fas fa-plus"></span></a>
                            </h3>
                            <div class="col-md-9">
                                <form class="form-inline float-right" method="GET" action="{{ route('orders.index') }}">
                                    <div class="form-group mx-sm-3">
                                    <label for="name" class="sr-only">Employee Name</label>
                                    <input class="form-control" placeholder="Employee Name" name="name" type="text" value="{{ request('name') }}" id="product">
                                    </div>
                                    <div class="form-group mx-sm-3">
                                        <label for="department" class="sr-only">Department</label>
                                        <select class="form-control" name="department" id="department">
                                            <option value="">Select department</option>
                                            {{--  @forelse($departments as $key => $department)
                                                <option {{ request('department') == $department->id ? "selected" : "" }} value="{{ $department->id }}">{{ $department->name }}</option>
                                            @empty
                                            @endforelse  --}}
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-round"><i class="fa fa-search"></i></button>
                                  </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped">
                                        <thead>
                                        <tr class="bg-secondary">
                                            <th class="text-center">SL</th>
                                            <th class="text-center">Order NO</th>
                                            <th class="text-center">Vehicle Details</th>
                                            <th class="text-center">Customer Details</th>
                                            <th class="text-center">Total Works</th>
                                            <th class="text-center">Total Parts</th>
                                            <th class="actions_column">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $sl = 0 @endphp
                                        @forelse($orders as $order)
                                            <tr>
                                                <td>{{ ++$sl }}</td>
                                                <td class="text-center">{{ $order->order_no }}</td>
                                                <td class="text-center">{{ $order->vehicle->name }}</td>
                                                <td class="text-center">{{ $order->customer->name }}</td>
                                                <td class="text-center">{{ $order->work_list->count() }}</td>
                                                <td class="text-center">{{ $order->part_list->count() }}</td>
                                                <td class="text-center">
                                                <form action="#" method="post"
                                                    onsubmit=" return confirm('Are you sure to delete?')">
                                                    @csrf
                                                    @method('delete')
                                                    <div class="btn-group">
                                                        {{--  <button type="button" class="btn btn-info salary_button" data-monthly-amount="{{ $order->salary }}" data-department-name="{{ $order->department->name }}" data-order-id="{{ $order->id }}" data-order-name="{{ $order->name }}" data-toggle="modal" data-target="#modal-sm"><i class="fas fa-handshake"></i></button>  --}}
                                                        {{--  <a href="{{route('orders.show', $order->id)}}" class="btn btn-success"><i class="fa fa-eye"></i></a>  --}}
                                                        {{--  <a href="#" class="btn btn-success" data-toggle="tooltip" data-placement="auto top" title="Show Status"><i class="fa fa-list"></i></a>  --}}
                                                        {{--  <a href="{{ route('orders.edit',1) }}" class="btn btn-warning"><i class="fa fa-edit"></i></a>  --}}
                                                        {{--  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>  --}}
                                                    </div>
                                                </form>
                                                </td>
                                            </tr>
                                        @empty

                                        @endforelse
                                            
                                        </tbody>
                                    </table>
                                    {{-- {{ $products->appends(request()->query())->links() }} --}}
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
             <form action="{{ route('salaries.store') }}" method="post">
        @csrf
        <div class="modal fade" id="modal-sm" style="display: none;" aria-hidden="true">
            <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Pay Salary</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                </div>
                <div class="modal-body">
                <div class="card-body box-profile pt-1">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{ asset('placeholders/avatar.png')}}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center mb-0" id="salary_order_name"></h3>
                <p class="text-muted text-center mb-0" id="salary_department_name"></p>
                <p class="text-center mb-0"><b>Monthly Salary : </b><span id="salary_monthly_amount"></span></p>
                <p class="text-center"><b>Due : </b> <span id="salary_monthly_amount_due">543</span></p>

                <div class="form-group">
                    <label for="year">Salary Year</label>
                    <select name="year" class="form-control" id="year" required>
                        <option value="2019">2019</option>
                        <option value="2020" selected>2020</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Salary Month</label>
                    <select name="month" class="form-control" id="month" required>
                        <option value='0'>January</option>
                        <option value='1'>February</option>
                        <option value='2'>March</option>
                        <option value='3'>Aprial</option>
                        <option value='4'>May</option>
                        <option value='5'>June</option>
                        <option value='6'>July</option>
                        <option value='7'>August</option>
                        <option value='8'>September</option>
                        <option value='9'>October</option>
                        <option value='10'>November</option>
                        <option value='11'>December</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Salary Amount</label>
                    <input type="number" class="form-control" name="amount" id="" placeholder="Enter amount" required>
                    <input type="hidden" name="order_id" id="salary_order_id"/>
                </div>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Pay Now</button>
                </div>
            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </form>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('script')
    <script>
        $('.salary_button').on('click',function(){
            $('#salary_order_id').val($(this).attr("data-order-id"));
            $('#salary_order_name').text($(this).attr("data-order-name"));
            $('#salary_department_name').text($(this).attr("data-department-name"));
            $('#salary_monthly_amount').text("TK. " + $(this).attr("data-monthly-amount"));
            {{--  $('#salary_monthly_amount_due').text($(this).attr("data-monthly-amount-due"));  --}}
        })
    </script>
@endsection