@extends('layouts.fixed')
@section('title', 'Order')
@section('content')
<section class="content mt-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- form start -->
                <form role="form" action="{{ route('orders.update', $employee->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Employee Edit Form</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- general form elements -->
                                    <div class="box box-info">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"></h3>
                                        </div>
                                        <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-2 control-label">Name</label>

                                                    <div class="col-sm-10">
                                                        <input type="text" name="name" class="form-control" id="name" value="{{old('name') ?? $employee->user->name }}" placeholder="Name">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="phone" class="col-sm-2 control-label">Phone</label>

                                                    <div class="col-sm-10">
                                                        <input type="phone" name='phone' class="form-control" id="phone" value="{{old('phone') ?? $employee->user->phone }}" placeholder="Phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="salary" class="col-sm-2 control-label">Salary</label>

                                                    <div class="col-sm-10">
                                                        <input type="salary" name='salary' class="form-control" id="salary" value="{{old('salary') ?? $employee->salary }}" placeholder="Salary">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="phone" class="col-sm-2 control-label">Department</label>

                                                    <div class="col-sm-10">
                                                        <select name="department_id" id='department_id'
                                                            class='form-control'>
                                                            <option>Select Department</option>
                                                            @forelse($departments as $department)
                                                            <option {{ $employee->department_id == $department->id ? "selected" : ""}} value='{{ $department->id }}'>{{ $department->name}}</option>
                                                            @empty

                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail3"
                                                        class="col-sm-2 control-label">Email</label>

                                                    <div class="col-sm-10">
                                                        <input type="email" name='email' class="form-control" value="{{old('email') ?? $employee->user->email}}"  id="inputEmail3" placeholder="Email" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail3"
                                                        class="col-sm-2 control-label">Address</label>
                                                    <div class="col-sm-10">
                                                        <textarea name='address' class="form-control" rows="3" placeholder="Enter Address...">{{old('address') ?? $employee->user->address }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPassword3" name='password'
                                                        class="col-sm-2 control-label">Password</label>

                                                    <div class="col-sm-10">
                                                        <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="avatar" class="col-sm-2 control-label">Avatar</label>

                                                    <div class="col-sm-10">
                                                        <input type="file" class="form-control-file" id="avatar">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-footer"></div>
                                            <!-- /.box-footer -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ route('orders.index') }}" class="btn btn-danger">Cancel</a>
                            <button type='reset' class="btn btn-primary">Reset Form</button>
                            <button type="submit" class="btn btn-success float-right">Update</button>
                        </div>
                    </div><!-- /.card -->
                </form>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('script')
<script>

</script>
@endsection
