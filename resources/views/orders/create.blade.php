@extends('layouts.fixed')
@section('title', 'Order Create')
@section('breadcrumb')
    <li class="breadcrumb-item active">Orders Create</li>
@stop
@section('content')
<section class="content mt-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- form start -->
                <form role="form" action="{{ route('orders.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Order Register Form</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- general form elements -->
                                    <div class="box box-info">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"></h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <div class="box-body">
                                            <div class="row" >
                                                <div class="callout callout-warning col">
                                                    <h5>Customer Information <a title="Click to add new customer" class="btn btn-sm text-info" href="{{ route('customers.create')}}"><span class="fas fa-plus"></span></a></h5>
                                                    <div class="form-group row">
                                                        <label for="customer_id" class="col-4 control-label">Customer Phone</label>
                                                        <div class="col-8">
                                                            <select type="text" name="customer_id" class="form-control" id="customer_id" required>
                                                                <option value="" disabled selected>Select Customer</option>
                                                                @forelse($customers as $customer)
                                                                    <option value="{{ $customer->id }}" >{{ $customer->phone .' | '. $customer->name }}</option>
                                                                @empty

                                                                @endforelse
                                                            </select>
                                                            @if($customers->count() == 0)
                                                                <span class="text-danger">No Customer Registered. <br>Please Register customer first</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card bg-light col">
                                                    <div class="card-body pt-0">
                                                        <h2 class="lead"><b>Nicole Pearson</b></h2>
                                                        <p class="text-muted text-sm"><b>About: </b> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: Demo Street 123, Demo City 04312, NJ</li>
                                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: + 800 - 12 12 23 52</li>
                                                        </ul>
                                                    </div>   
                                                </div>
                                            </div>
                                            <div class="row" >
                                                <div class="callout callout-warning col">
                                                    <h5>Vehicle Information <a title="Click to add new vehicle" class="btn btn-sm text-info" href="{{ route('vehicles.create')}}"><span class="fas fa-plus"></span></a></h5>
                                                    <div class="form-group row">
                                                        <label for="vehicle_id" class="col-4 control-label">Vehicle Plate No</label>
                                                        <div class="col-8">
                                                            <select type="text" name="vehicle_id" class="form-control" id="vehicle_id" required>
                                                                <option value="" disabled selected>Select Vehicle</option>
                                                                @forelse($vehicles as $vehicle)
                                                                    <option value="{{ $vehicle->id }}" >{{ $vehicle->no .' | '. $vehicle->name }}</option>
                                                                @empty

                                                                @endforelse
                                                            </select>
                                                            @if($vehicles->count() == 0)
                                                                <span class="text-danger">No Vehicle Registered. <br>Please Register vehicle first</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card bg-light col">
                                                    <div class="card-body pt-0">
                                                        <h2 class="lead"><b>Nicole Pearson</b></h2>
                                                        <p class="text-muted text-sm"><b>About: </b> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: Demo Street 123, Demo City 04312, NJ</li>
                                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: + 800 - 12 12 23 52</li>
                                                        </ul>
                                                    </div>   
                                                </div>
                                            </div>
                                            <div class="row" >
                                                <div class="callout callout-warning col">
                                                    <h5>Other Information</h5>
                                                    <div class="row" >
                                                        <div class="form-group col">
                                                            <label for="delivery_date" class="col-sm-6 control-label">Delivery Date</label>

                                                            <div class="col-sm-10">
                                                                <input type="date" name="delivery_date" class="form-control" id="delivery_date" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col">
                                                            <label for="name" class="col-sm-6 control-label">Customer Note</label>

                                                            <div class="col-sm-10">
                                                                <textarea name="customer_note" class="form-control" id="customer_note" placeholder="Enter Customer Note"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer col-12">
                                    <a href="{{ route('orders.index') }}" class="btn btn-danger">Cancel</a>
                                    <button type='reset' class="btn btn-primary">Reset Form</button>
                                    <button type="submit" class="btn btn-success float-right">Register</button>
                                </div>
                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div><!-- /.card-body -->
                        <div class="card-footer"></div>
                    </div><!-- /.card -->
                </form>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('script')
<script>

</script>
@endsection
