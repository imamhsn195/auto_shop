@extends('layouts.fixed')
@section('title','Order Details')
@section('breadcrumb')
    <li class="breadcrumb-item active">Employee Details</li>
@endsection
@section('content')
    <section class="content mt-1">
        <div class="container-fluid">
            <div class="row">
            <div class="col-md-12 mt-3">
            <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header text-white" style="background: url('../dist/img/photo1.png') center center;">
                <h3 class="widget-user-username text-right">{{ $employee->name }}</h3>
                <h5 class="widget-user-desc text-right">{{ $employee->department->name }}</h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle" src="{{asset('placeholders/avatar.png')}}" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">Join Date</h5>
                      <span class="description-text">{{ $employee->joining_date ?? "Not Available"}}</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">Total Presents</h5>
                      <span class="description-text">{{ $employee->presents() }} days</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header">Total Absents</h5>
                      <span class="description-text">{{ $employee->absents() }} days</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
                <div class="col-md-12">
                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Employment Information of {{$employee->name}}</h3>
                        </div>
                        <!-- /.edit-form -->
                        <!-- /.card-header -->
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped">
                                            <thead>
                                                <tr class="bg-secondary">
                                                    <th width="20px">SL</th>
                                                    <th>Month</th>
                                                    <th>Year</th>
                                                    <th class="float-right">Amount</th>
                                                    {{--  <th class="actions_column">Actions</th>  --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $salary_sl = 0 @endphp
                                            @forelse($employee->salaries as $key => $salary)
                                                <tr>
                                                    <td>{{ ++ $salary_sl}}</td>
                                                    <td>{{ getMonthName($salary->month) }}</td>
                                                    <td>{{ $salary->year }}</td>
                                                    <td class="float-right">{{ number_format($salary->amount,2)}}</td>
                                                    {{--  <td></td>  --}}
                                                </tr>
                                                @empty
                                                <tr>
                                                  <td colspan="4"> No Salary record found </td>
                                                </tr>
                                            @endforelse
                                            </tbody>
                                            <tfoot>
                                              <tr>
                                                <td colspan="4" style="text-align:end">Total Salary Paid : Tk. {{ number_format($employee->salaries->sum('amount'),2) }}</td>
                                              </tr>
                                              <tr>
                                                <td colspan="4" style="text-align:end">Total Salary Due : Tk. {{ number_format($employee->salaries->sum('amount'),2) }}</td>
                                              </tr>
                                            </tfoot>
                                        </table>
                                        {{-- {{ $salaries->appends(request()->query())->links() }} --}}
                                    </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                            </div>
                            <!-- /.card-body -->
                        </form>
                        <!-- /.edit-form -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>

    </script>
@endsection