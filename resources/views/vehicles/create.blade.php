@extends('layouts.fixed')
@section('title', 'Vehicle Create')
@section('content')
<section class="content mt-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- form start -->
                <form role="form" action="{{ route('vehicles.store') }}" method="post" enctype="multipart/form-data">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Vehicle Register Form</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- general form elements -->
                                    <div class="box box-info">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"></h3>
                                        </div>
                                        <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="form-group row">
                                                <label for="name" class="col-sm-2 control-label">Name</label>
                            
                                                <div class="col-sm-10">
                                                <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="no" class="col-sm-2 control-label">Plate No</label>
                            
                                                <div class="col-sm-10">
                                                <input type="text" name="no" class="form-control" id="no" placeholder="Plate No">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="cc" class="col-sm-2 control-label">Vehicle Owner</label>
                            
                                                <div class="col-sm-10">
                                                    <select name="customer_id" id="customer_id" class="form-control">
                                                        <option value="" selected disabled>Select Customer</option>
                                                        @forelse($customers as $customer)
                                                            <option value="{{ $customer->id }}">{{ $customer->phone . " | " . $customer->name }}</option>
                                                        @empty

                                                        @endforelse
                                                    </select>
                                                    @if($customers->count() == 0)
                                                        <span class="text-danger">There is no customer. Please Register customer first</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="driver_name" class="col-sm-2 control-label">Driver Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="driver_name" class="form-control" id="driver_name" placeholder="Enter Driver Name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="driver_phone" class="col-sm-2 control-label">Driver Phone</label>
                            
                                                <div class="col-sm-10">
                                                <input type="text" class="form-control" name="driver_phone" id="driver_phone" placeholder="Enter Driver Phone">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="color" class="col-sm-2 control-label">Color</label>
                            
                                                <div class="col-sm-10">
                                                <input type="text" class="form-control" name="color" id="color" placeholder="Enter Body Color">
                                                </div>
                                            </div>
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-footer"></div>
                                            <!-- /.box-footer -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ route('vehicles.index') }}" class="btn btn-danger">Cancel</a>
                            <button type='reset' class="btn btn-primary">Reset Form</button>
                            <button type="submit" class="btn btn-success float-right">Register</button>
                        </div>
                    </div><!-- /.card -->
                </form>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('script')
<script>

</script>
@endsection
