@extends('layouts.fixed')
@section('title', 'Parts')
@section('breadcrumb')
    <li class="breadcrumb-item active">Parts</li>
@endsection
@section('content')
    <section class="content mt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary col-md-12">
                        <div class="card-header row">
                            <h3 class="card-title col-md-3">All Parts <a title="Click to add new part" class="btn btn-sm " href="{{ route('parts.create')}}">
                                <span class="fas fa-plus"></span></a>
                            </h3>
                            <div class="col-md-9">
                                <form class="form-inline float-right" method="GET" action="{{ route('parts.index') }}">
                                    <div class="form-group mx-sm-3">
                                      <label for="name" class="sr-only">Name</label>
                                    <input class="form-control" placeholder="Product Name" name="name" type="text" value="{{ request('name') }}" id="product">
                                    </div>
                                    <div class="form-group mx-sm-3">
                                        <label for="category" class="sr-only">Category</label>
                                        <select class="form-control" name="category" id="category">
                                            <option value="">Select Category</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-round"><i class="fa fa-search"></i></button>
                                  </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped">
                                        <thead>
                                        <tr class="bg-secondary">
                                            <th class="text-center">SL</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Supplier</th>
                                            <th class="text-center">Brand</th>
                                            <th class="text-center">Unit</th>
                                            <th class="text-center">Purchase Price</th>
                                            <th class="text-center">Sale Price</th>
                                            <th class="actions_column">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $sl = 0 @endphp
                                        @forelse($parts as $part)
                                            <tr>
                                            <td>{{ ++$sl }}</td>
                                                <td class="text-center">{{ $part->name }}</td>
                                                <td class="text-center">{{ $part->supplier->name }}</td>
                                                <td class="text-center">{{ $part->brand->name }}</td>
                                                <td class="text-center">{{ $part->unit->name }}</td>
                                                <td class="text-center">{{ number_format($part->purchase_price,2) }}</td>
                                                <td class="text-center">{{ number_format($part->sale_price,2) }}</td>
                                                <td class="text-center">
                                                    <form action="#" method="post" onsubmit=" return confirm('Are you sure to delete?')">
                                                        @csrf
                                                        @method('delete')
                                                        <div class="btn-group">
                                                            {{--  <a href="{{route('parts.show', $part->id)}}" class="btn btn-success"><i class="fa fa-eye"></i></a>  --}}
                                                            {{--  <a href="#" class="btn btn-success" data-toggle="tooltip" data-placement="auto top" title="Show Status"><i class="fa fa-list"></i></a>  --}}
                                                            <a href="{{ route('parts.edit',1) }}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                                            {{--  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>  --}}
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty

                                        @endforelse
                                            
                                        </tbody>
                                    </table>
                                    {{-- {{ $products->appends(request()->query())->links() }} --}}
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
             <form action="{{ route('salaries.store') }}" method="post">
        @csrf
        <div class="modal fade" id="modal-sm" style="display: none;" aria-hidden="true">
            <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Pay Salary</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                </div>
                <div class="modal-body">
                <div class="card-body box-profile pt-1">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{ asset('placeholders/avatar.png')}}" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center mb-0" id="salary_part_name"></h3>
                <p class="text-muted text-center mb-0" id="salary_department_name"></p>
                <p class="text-center mb-0"><b>Monthly Salary : </b><span id="salary_monthly_amount"></span></p>
                <p class="text-center"><b>Due : </b> <span id="salary_monthly_amount_due">543</span></p>

                <div class="form-group">
                    <label for="year">Salary Year</label>
                    <select name="year" class="form-control" id="year" required>
                        <option value="2019">2019</option>
                        <option value="2020" selected>2020</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Salary Month</label>
                    <select name="month" class="form-control" id="month" required>
                        <option value='0'>January</option>
                        <option value='1'>February</option>
                        <option value='2'>March</option>
                        <option value='3'>Aprial</option>
                        <option value='4'>May</option>
                        <option value='5'>June</option>
                        <option value='6'>July</option>
                        <option value='7'>August</option>
                        <option value='8'>September</option>
                        <option value='9'>October</option>
                        <option value='10'>November</option>
                        <option value='11'>December</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Salary Amount</label>
                    <input type="number" class="form-control" name="amount" id="" placeholder="Enter amount" required>
                    <input type="hidden" name="part_id" id="salary_part_id"/>
                </div>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Pay Now</button>
                </div>
            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </form>
        </div><!-- /.container-fluid -->
    </section>
@endsection
@section('script')
    <script>
        $('.salary_button').on('click',function(){
            $('#salary_part_id').val($(this).attr("data-part-id"));
            $('#salary_part_name').text($(this).attr("data-part-name"));
            $('#salary_department_name').text($(this).attr("data-department-name"));
            $('#salary_monthly_amount').text("TK. " + $(this).attr("data-monthly-amount"));
            {{--  $('#salary_monthly_amount_due').text($(this).attr("data-monthly-amount-due"));  --}}
        })
    </script>
@endsection