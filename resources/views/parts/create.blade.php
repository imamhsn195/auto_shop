@extends('layouts.fixed')
@section('title', 'Parts')
@section('content')
<section class="content mt-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- form start -->
                <form role="form" action="{{ route('parts.store') }}" method="post" enctype="multipart/form-data">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Parts Register Form</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- general form elements -->
                                    <div class="box box-info">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"></h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <form action="{{ route('parts.store') }}" method="post">
                                            @csrf
                                            <div class="box-body">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-2 control-label">Name</label>

                                                    <div class="col-sm-10">
                                                        <input type="text" name="name" class="form-control" id="name" placeholder="Name" required>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="supplier_id" class="col-sm-2 control-label">Supplier</label>

                                                    <div class="col-sm-10">
                                                        <select name="supplier_id" id='supplier_id' class='form-control' required>
                                                            <option selected disabled value=''>Select Supplier</option>
                                                            @forelse($suppliers as $supplier)
                                                                <option value='{{ $supplier->id }}'>{{ $supplier->name }}</option>
                                                            @empty
                                                            
                                                            @endforelse
                                                        </select>
                                                        @if($suppliers->count() == 0)
                                                            <span class="text-danger">There is no supplier. Please Register supplier first</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="unit_id" class="col-sm-2 control-label">Unit</label>

                                                    <div class="col-sm-10">
                                                        <select name="unit_id" id='unit_id' class='form-control' required>
                                                            <option selected disabled value=''>Select Unit</option>
                                                            @forelse($units as $unit)
                                                                <option value='{{ $unit->id }}'>{{ $unit->name }}</option>
                                                            @empty

                                                            @endforelse
                                                        </select>
                                                        @if($units->count() == 0)
                                                            <span class="text-danger">There is no unit. Please Register unit first</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="brand_id" class="col-sm-2 control-label">Brand</label>

                                                    <div class="col-sm-10">
                                                        <select name="brand_id" id='brand_id' class='form-control' required>
                                                            <option selected disabled value=''>Select Brand</option>
                                                            @forelse($brands as $brand)
                                                                <option value='{{ $brand->id }}'>{{ $brand->name}}</option>
                                                            @empty
                                                            
                                                            @endforelse
                                                        </select>
                                                        @if($brands->count() == 0)
                                                        <span class="text-danger">There is no brand. Please Register brand first</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="purchase_price" class="col-sm-2 control-label">Purchase Price</label>

                                                    <div class="col-sm-10">
                                                        <input type="purchase_price" name='purchase_price' class="form-control"
                                                            id="purchase_price" placeholder="Enter Purchase Price">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="sale_price" class="col-sm-2 control-label">Sale Price</label>

                                                    <div class="col-sm-10">
                                                        <input type="sale_price" name='sale_price' class="form-control"
                                                            id="sale_price" placeholder="Enter Sale Price">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-footer"></div>
                                            <!-- /.box-footer -->
                                        </form>
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ route('parts.index') }}" class="btn btn-danger">Cancel</a>
                            <button type='reset' class="btn btn-primary">Reset Form</button>
                            <button type="submit" class="btn btn-success float-right">Register</button>
                        </div>
                    </div><!-- /.card -->
                </form>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('script')
<script>

</script>
@endsection
