@extends('layouts.fixed')
@section('title', 'Employees')
@section('breadcrumb')
    <li class="breadcrumb-item active">Employee Update</li>
@endsection
@section('content')
<section class="content mt-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- form start -->
                <form role="form" action="{{ route('customers.update', $customer->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Employee Update Form</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- general form elements -->
                                    <div class="box box-info">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"></h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <div class="box-body">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-2 control-label">Name</label>

                                                <div class="col-sm-10">
                                                    <input type="text" name="name" value="{{old('name') ?? $customer->name}}" class="form-control" id="name" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="phone" class="col-sm-2 control-label">Phone</label>

                                                <div class="col-sm-10">
                                                    <input type="phone" name='phone' value="{{old('phone') ?? $customer->phone}}"  class="form-control" id="phone" placeholder="Phone">
                                                </div>
                                            </div>
                                    
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                                                <div class="col-sm-10">
                                                    <input type="email" name='email' value="{{old('email') ?? $customer->email}}"  class="form-control" id="inputEmail3" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                                                <div class="col-sm-10">
                                                    <textarea name='address' class="form-control" rows="3" placeholder="Enter Address...">{{old('address') ?? $customer->address}}</textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                {{--  <label for="avatar" class="col-sm-2 control-label">Avatar</label>  --}}

                                                <div class="col-sm-10">
                                                    {{--  <input type="file" class="form-control-file" id="avatar">  --}}
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer"></div>
                                        <!-- /.box-footer -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ route('employees.index') }}" class="btn btn-danger">Cancel</a>
                            <a href="{{ route('customers.index') }}" class="btn btn-primary">Reset Form</a>
                            <button type="submit" class="btn btn-success float-right">Update</button>
                        </div>
                    </div><!-- /.card -->
                </form>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('script')
<script>

</script>
@endsection
