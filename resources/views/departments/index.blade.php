@extends('layouts.fixed')
@section('title', 'Departments')
@section('breadcrumb')
    <li class="breadcrumb-item active">Departments</li>
@endsection
@section('content')
    <section class="content mt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 no-print">
                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title mb-3">{{ $update_department_info == null ? "Create department" : "Update department( $update_department_info->name )" }}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                                @if ($update_department_info !== null)
                                    <form role="form" action="{{ route('departments.update',$update_department_info->id) }}" method="post" enctype="multipart/form-data">
                                    @method('put')
                                @else
                                    <form role="form" action="{{ route('departments.store') }}" method="post" enctype="multipart/form-data">
                                @endif
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" value="{{ $update_department_info !== null ? $update_department_info->name : '' }}" class="@error('name') is-invalid @enderror form-control" id="name" placeholder="Enter Name" required>
                                        @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="@error('description') is-invalid @enderror form-control"  name="description" id="description" placeholder="Short Description">{{ $update_department_info !== null ? $update_department_info->description : '' }}</textarea>
                                        @error('description')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('departments.index') }}" class="btn btn-primary">Reset</a>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card card-primary col-md-12">
                        <div class="card-header row">
                            <h3 class="card-title col-md-3 ">All Departments</h3>
                            <div class="col-md-9">
                                <form class="form-inline float-right" method="GET" action="{{ route('departments.index') }}">
                                    <div class="form-group mx-sm-3">
                                      <label for="name" class="sr-only">Name</label>
                                    <input class="form-control" placeholder="Search Department" name="name" type="text" value="{{ request('name') }}" id="product">
                                    </div>
                                    <button type="submit" class="btn btn-info btn-round"><i class="fa fa-search"></i></button>
                                  </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th class="actions_column no-print">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($departments as $key => $department)
                                            <tr>
                                                <td>{{ (($departments->currentPage() - 1) * $departments->perPage() + $key+1) }}</td>
                                                <td>{{ $department->name }}</td>
                                                <td class="text-center  no-print">
                                                    <form action="{{ route('departments.destroy', $department->id) }}" method="post" onsubmit="return confirm('Are you sure to delete?')">
                                                        @method('delete')
                                                        @csrf
                                                    <div class="btn-group">
                                                        {{--  <a href="#" class="btn btn-success"><i class="fa fa-eye"></i></a>  --}}
                                                        {{--  <a href="#" class="btn btn-success" data-toggle="tooltip" data-placement="auto top" title="Show Status"><i class="fa fa-list"></i></a>  --}}
                                                        <a href="{{ route('departments.index',['update_department_id' => $department->id]) }}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                                        {{--  <button type="submit" class="btn btn-danger" onclick="return confirm('are you sure to delete!')"><i class="fa fa-trash"></i></button>  --}}
                                                    </div>
                                                </form>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr><td colspan="9" class="text-center"> No record found </td></tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                    {{ $departments->appends(request()->query())->links() }}
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection