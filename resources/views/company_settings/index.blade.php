@extends('layouts.fixed')
@section('title','Company Settings')
@section('breadcrumb')
    <li class="breadcrumb-item active">Company Settings</li>
@endsection
@section('content')
    <section class="content mt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" src="{{ company()->logo }}" alt="User profile picture">
                            </div>

                            <h3 class="profile-username text-center">&nbsp;{{ company()->name ?? 'Company Name' }}</h3>
                            <p class="text-muted text-center">{{ company()->message ?? "A comfortable life maker" }}</p>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">About {{  company()->name ?? 'Company Name' }}</h3>
                        </div>
                        <!-- /.edit-form -->
                        <form action="{{ route('company_settings.update', company()->id ?? 1) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                        <!-- /.card-header -->
                            <div class="card-body">
                                <strong><i class="fa fa-industry"></i> Name</strong>

                                <p class="text-muted">
                                    {{  company()->name ?? 'Company Name' }}
                                </p>
                                <input type="text" name="name" class="form-control" value="{{  company()->name ?? 'Company Name' }}" placeholder="Enter Company Name"/>
                                <hr>
                                <strong><i class="fa fa-sms"></i> Message</strong>

                                <p class="text-muted">
                                    {{  company()->message ?? 'A comfortable life maker' }}
                                </p>
                                <input type="text" name="message" class="form-control" value="{{  company()->message ?? 'A comfortable life maker' }}" placeholder="Enter Company Name"/>
                                <hr>
                                <strong><i class="fas fa-envelope mr-1"></i> Email</strong>

                                <p class="text-muted">
                                    {{  company()->email ?? 'company@domain.com' }}
                                </p>
                                <input type="text" name="email" class="form-control" value="{{  company()->email ?? 'company@domain.com' }}" placeholder="Enter Company Email"/>
                                <hr>

                                <strong><i class="fas fa-phone mr-1"></i> Phone</strong>

                                <p class="text-muted">
                                    {{  company()->phone ?? '0000-000000' }}
                                </p>
                                    <input type="text" name="phone" class="form-control" value="{{  company()->phone ?? '0000-000000' }}" placeholder="Enter Company Email"/>
                                <hr>

                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                                <p class="text-muted">
                                    {{  company()->address ?? 'Malibu, California' }}
                                </p>
                                <textarea name="address" id="" cols="30" rows="5" class="form-control">{{  company()->address ?? 'Malibu, California' }}</textarea>

                                <hr>

                                <strong><i class="fas fa-landmark mr-1"></i> Logo</strong>
                                <div class="form-group ml-4">
                                    <input type="file"  name="logo" class="form-control-file">
                                </div>
                            </div>
                            <div class="form-group ml-4">
                                <input type="submit" value="Update" class="btn btn-primary">
                            </div>
                            <!-- /.card-body -->
                        </form>
                        <!-- /.edit-form -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>

    </script>
@endsection