@extends('layouts.fixed')
@section('title', 'Employees')
@section('content')
<section class="content mt-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- form start -->
                <form role="form" action="{{ route('employees.store') }}" method="post" enctype="multipart/form-data">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Employee Register Form</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- general form elements -->
                                    <div class="box box-info">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"></h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <form action="{{ route('employees.store') }}" method="post">
                                            @csrf
                                            <div class="box-body">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-2 control-label">Name</label>

                                                    <div class="col-sm-10">
                                                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter Employee Name">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="phone" class="col-sm-2 control-label">Phone</label>

                                                    <div class="col-sm-10">
                                                        <input type="phone" name='phone' class="form-control" id="phone"placeholder="Enter Employee Phone">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="joining_date" class="col-sm-2 control-label">Join Date</label>

                                                    <div class="col-sm-10">
                                                        <input type="date" name='joining_date' class="form-control" id="joining_date">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="salary" class="col-sm-2 control-label">Salary</label>

                                                    <div class="col-sm-10">
                                                        <input type="salary" name='salary' class="form-control"id="salary" placeholder="Enter Monthly Salary">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="phone" class="col-sm-2 control-label">Department</label>

                                                    <div class="col-sm-10">
                                                        <select name="department_id" id='department_id' class='form-control'>
                                                            <option selected disabled>Select Department</option>
                                                            @forelse($departments as $department)
                                                            <option value='{{ $department->id }}'>
                                                                {{ $department->name}}</option>
                                                            @empty

                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="email"class="col-sm-2 control-label">Email</label>

                                                    <div class="col-sm-10">
                                                        <input type="email" name='email' class="form-control" id="email" placeholder="Enter Employee Email">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="address"
                                                        class="col-sm-2 control-label">Address</label>
                                                    <div class="col-sm-10">
                                                        <textarea name='address' class="form-control" rows="3" placeholder="Enter Employee Address..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            <div class="box-footer"></div>
                                            <!-- /.box-footer -->
                                        </form>
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ route('employees.index') }}" class="btn btn-danger">Cancel</a>
                            <button type='reset' class="btn btn-primary">Reset Form</button>
                            <button type="submit" class="btn btn-success float-right">Register</button>
                        </div>
                    </div><!-- /.card -->
                </form>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('script')
<script>

</script>
@endsection
