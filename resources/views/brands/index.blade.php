@extends('layouts.fixed')
@section('title', 'Brand')
@section('breadcrumb')
    <li class="breadcrumb-item active">Brands</li>
@endsection
@section('content')
    <section class="content mt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 no-print">
                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title mb-3">{{ $update_brand_info == null ? "Create Brand" : "Update Brand( $update_brand_info->name )" }}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                                @if ($update_brand_info !== null)
                                    <form role="form" action="{{ route('brands.update',$update_brand_info->id) }}" method="post" enctype="multipart/form-data">
                                    @method('put')
                                @else
                                    <form role="form" action="{{ route('brands.store') }}" method="post" enctype="multipart/form-data">
                                @endif
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" value="{{ $update_brand_info !== null ? $update_brand_info->name : '' }}" class="@error('name') is-invalid @enderror form-control" id="name" placeholder="Enter Name" required>
                                        @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('brands.index') }}" class="btn btn-primary">Reset</a>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card card-primary col-md-12">
                        <div class="card-header row">
                            <h3 class="card-title col-md-3">Registered Brand</h3>
                            <div class="col-md-9">
                                <form class="form-inline float-right" method="GET" action="{{ route('employees.index') }}">
                                    <div class="form-group mx-sm-3">
                                      <label for="name" class="sr-only">Name</label>
                                    <input class="form-control" placeholder="Product Name" name="name" type="text" value="{{ request('name') }}" id="product">
                                    </div>
                                    <div class="form-group mx-sm-3">
                                        <label for="category" class="sr-only">Category</label>
                                        <select class="form-control" name="category" id="category">
                                            <option value="">Select Category</option>
                                            {{--  @forelse($categories as $key => $category)
                                                <option {{ request('category') == $category->id ? "selected" : "" }} value="{{ $category->id }}">{{ $category->name }}</option>
                                            @empty
                                            @endforelse  --}}
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-round"><i class="fa fa-search"></i></button>
                                  </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th class="actions_column no-print">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($brands as $key => $brand)
                                            <tr>
                                                <td>{{ (($brands->currentPage() - 1) * $brands->perPage() + $key+1) }}</td>
                                                <td>{{ $brand->name }}</td>
                                                <td class="text-center  no-print">
                                                    <form action="{{ route('brands.destroy', $brand->id) }}" method="post">
                                                        @method('delete')
                                                        @csrf
                                                        <a href="{{ route('brands.index',['update_brand_id' => $brand->id]) }}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                                        {{--  <button type="submit" class="btn btn-danger" onclick="return confirm('are you sure to delete!')"><i class="fa fa-trash"></i></button>  --}}
                                                    </form>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr><td colspan="9" class="text-center"> No record found </td></tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                    {{ $brands->appends(request()->query())->links() }}
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection