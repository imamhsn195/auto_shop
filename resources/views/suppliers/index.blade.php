@extends('layouts.fixed')
@section('title', 'Supplier')

@section('breadcrumb')
    <li class="breadcrumb-item active">Suppliers</li>
@endsection
@section('content')
    <section class="content mt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary col-md-12">
                        <div class="card-header row">
                            <h3 class="card-title col-md-3">All Suppliers <a title="Click to add new customer" class="btn btn-sm " href="{{ route('suppliers.create')}}">
                                <span class="fas fa-plus"></span></a>
                            </h3>
                            <div class="col-md-9">
                                <form class="form-inline float-right" method="GET" action="{{ route('suppliers.index') }}" accept-charset="UTF-8">
                                    <div class="form-group mx-sm-3">
                                      <label for="name" class="sr-only">Name</label>
                                    <input class="form-control" placeholder="Supplier Name" name="name" type="text" value="{{ request('name') }}" id="name">
                                    </div>
                                    <button type="submit" class="btn btn-info btn-round"><i class="fa fa-search"></i></button>
                                    @if(request()->query())
                                        <a href="{{ route('suppliers.index') }}" class='btn btn-info btn-round' >All</a>
                                    @endif
                                  </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped">
                                        <thead>
                                        <tr class='bg-secondary'>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Address</th>
                                            <th class="text-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php $sl = 0 @endphp
                                        @forelse($suppliers as $customer)
                                        <tr>
                                            <td>{{ ++$sl }}</td>
                                            <td>{{ $customer->name}}</td>
                                            <td>{{ $customer->phone }}</td>
                                            <td>{{ $customer->email }}</td>
                                            <td>{{ $customer->address }}</td>
                                            <td class="text-center">
                                                <form action="#" method="post"
                                                    onsubmit=" return confirm('Are you sure to delete?')">
                                                    @csrf
                                                    @method('delete')
                                                    <div class="btn-group">
                                                        {{--  <a href="#" class="btn btn-success"><i class="fa fa-eye"></i></a>  --}}
                                                        {{--  <a href="#" class="btn btn-success" data-toggle="tooltip" data-placement="auto top" title="Show Status"><i class="fa fa-list"></i></a>  --}}
                                                        <a href="{{ route('suppliers.edit',1) }}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                                        {{--  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>  --}}
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                        @empty

                                        @endforelse
                                            
                                        </tbody>
                                    </table>
                                    {{-- {{ $products->appends(request()->query())->links() }} --}}
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection