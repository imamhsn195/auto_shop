@extends('layouts.fixed')
@section('title', 'Employees')
@section('content')
<section class="content mt-1">
    <div class="container-fluid">
        <form class="row" method="post" action="{{ route('attendances.store') }}">
        @csrf
            <div class="col-md-12">
                <div class="card card-primary col-md-12">
                    <div class="card-header row">
                        <h3 class="card-title col-md-3">Attendance Registration
                         {{--  <a title="Click to add new product" class="btn btn-sm " href="{{ route('employees.create')}}"> <span class="fas fa-plus"></span></a>  --}}
                        </h3>
                        <div class="col-md-9">
                            <div class="form-inline float-right">
                                <div class="form-group mx-sm-3">
                                    <label for="date" class="sr-only">date</label>
                                    <input class="form-control" placeholder="Product date" name="date" type="date" value="{{ request('date') }}" id="product" required>
                                </div>
                                <button type="submit" class="btn btn-info btn-round">Save Attendance</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                    <div class="col-md-12">
                        <table class='table table-sm table-bordered'>
                            <thead>
                                <tr class='bg-secondary'>
                                    <td>SL#</td>
                                    <td>Employee Name</td>
                                    <td>Department</td>
                                    <td>Attendance</td>
                                </tr>
                            </thead>
                            <tbody>
                            @php $sl = 0 @endphp
                            @forelse($employees as $employee)
                                <tr>
                                    <td>{{ ++$sl}}</td>                                
                                    <td>{{ $employee->name}}</td>                                
                                    <td>{{ $employee->department->name}}</td>
                                    <td>
                                    <input type="radio" name='attendances[{{$employee->id}}]' value="p"  class='form-control-sm' required="required"/> Present
                                    <input type="radio" name='attendances[{{$employee->id}}]' value="a"  class='form-control-sm' required="required"/> Absent
                                    <input type="radio" name='attendances[{{$employee->id}}]' value="s"  class='form-control-sm' required="required"/> Sick
                                    </td>
                                </tr>
                                @empty
                            @endforelse
                            </tbody>
                            </div>
                        </table>
                    </div>
                </div>
            </div><!-- /.col-md-12 -->
        </form><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('script')
<script>

</script>
@endsection
