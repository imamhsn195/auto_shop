@extends('layouts.fixed')
@section('title', 'Products')
@section('breadcrumb')
    <li class="breadcrumb-item active">Attendance</li>
@endsection
@section('content')
    <section class="content mt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary col-md-12">
                        <div class="card-header row">
                            <h3 class="card-title col-md-3">Today Attendance <a title="Click to add new product" class="btn btn-sm " href="{{ route('attendances.create')}}">
                                <span class="fas fa-plus"></span></a>
                            </h3>
                            <div class="col-md-9">
                                <form class="form-inline float-right" method="GET" action="{{ route('attendances.index') }}">
                                    <div class="form-group mx-sm-3">
                                        <label for="date" class="sr-only">Date</label>
                                       <input type="date" name='date' value="{{ request('date') }}" class='form-control'/>
                                    </div>
                                    <div class="form-group mx-sm-3">
                                        <button type="submit" class="btn btn-info btn-round"><i class="fa fa-search"></i> search</button>
                                    </div>
                                  </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                    <div class="col-md-3 col-sm-6 col-12">
                                        <div class="info-box">
                                        <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Employees</span>
                                            <span class="info-box-number">{{ $attendances->count() }}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-12">
                                        <div class="info-box">
                                        <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Present</span>
                                            <span class="info-box-number">{{ $attendances->where('attendance','p')->count() }}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-12">
                                        <div class="info-box">
                                        <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Absents</span>
                                            <span class="info-box-number">{{ $attendances->where('attendance','a')->count() }}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-12">
                                        <div class="info-box">
                                        <span class="info-box-icon bg-danger"><i class="far fa-star"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Sicks</span>
                                            <span class="info-box-number">{{ $attendances->where('attendance','s')->count() }}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    </div>
       
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped">
                                        <thead>
                                        <tr class="bg-secondary">
                                            <th class="text-center">SL</th>
                                            <th>Employee Name</th>
                                            <th>Phone</th>
                                            <th class="text-center">Department</th>
                                            <th class="text-center">Attendance</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $sl = 0 @endphp
                                        @forelse($attendances as $attendance)
                                            <tr>
                                                <td class="text-center">{{ ++$sl }}</td>
                                                <td>{{ $attendance->employee->name }}</td>
                                                <td>{{ $attendance->employee->phone }}</td>
                                                <td class="text-center">{{ $attendance->employee->department->name }}</td>
                                                <td class="text-center">{{ $attendance->attendance }}</td>
                                            </tr>
                                        @empty

                                        @endforelse
                                            
                                        </tbody>
                                    </table>
                                    {{-- {{ $products->appends(request()->query())->links() }} --}}
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection