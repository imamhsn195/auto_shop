@extends('layouts.fixed')
@section('title','Reports | Salary')
@section('breadcrumb')
    <li class="breadcrumb-item active">Salary Report</li>
@endsection

@section('css')
    <style>
        .table tr td, .table tr th{
            border: none;
        }
        table.dataTable.row-border tbody th, table.dataTable.row-border tbody td, table.dataTable.display tbody th, table.dataTable.display tbody td {
            border-top: 0px solid #ddd!important;
        }
        .unitName {
            font-size: 21px;
            font-weight: bold;
        }
    </style>
@stop
@section('content')
    {{--start searching individual ledger report --}}
    <section class="content no-print">
        <div class="container-fluid">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                @if($errors->any())
                    <div class="col-md-12 alert alert-danger emptyMsg">
                        <h6>{{$errors->first(1)}}</h6>
                    </div>
                @endif
            </div>
        </div>{{--end --}}
    </section>
    {{--end searching individual ledger report --}}

    <!-- Main content -->
    <section class="content mt-1">
        <div class="col-lg-12">
            <div class="card-body">
                {{-- start showin gsuccess message --}}
                <div class="col-md-4">
                    @if (session('success'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
                {{-- end showin gsuccess message --}}

                    <div class="row">
                        <div class="col-lg-12">
                            <div class=" bg-light">
                                <div class="card-body" style="display: block; min-height: 800px">
                                    <div class="box-header with-border text-center" style="margin-bottom: -25px!important;">
                                        <h3 class="">{{ company()->name }}</h3>
                                        <p style="text-transform: capitalize;text-align: center">{{ company()->address }}</p>
                                        <h5 style="text-transform: capitalize;text-align: center">Employees Monthly Report</h5>
                                        @if(request()->query())
                                            <p>{{ getMonthName(request('month')) . ' of ' . request('year') }}</p><br>
                                            @else
                                            <b></b>
                                            <p></p><br>
                                        @endif
                                    </div>
                                    <form method="GET" action="{{ route('salaries.index') }}">
                                        <div class="form-row no-print">
                                            <div class="form-group col-md-2" style="margin: -2px 0 0 0;"></div>
                                            <div class="form-group col-md-3" style="margin: -2px 0 0 0;">
                                                <select class="form-control" name="month" required>
                                                    <option value="" disabled selected>Select Month</option>
                                                    @forelse(months() as $key => $month)
                                                        <option {{ (request('month') != '' && request('month') ==  $key) ? 'selected' : '' }} value="{{ $key }}">{{ $month }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3" style="margin: -2px 0 0 0;">
                                                <select class="form-control" name="year" required>
                                                    <option value="" disabled selected>Select Year</option>
                                                    @forelse(years() as $year)
                                                        <option {{ $year == '2020' ? 'selected' : ''}} value="{{ $year }}">{{ $year }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                            
                                            <div class="form-group col-md-2">
                                                {{--<label for="end" class="">To Date</label>--}}
                                                <input type="submit" class="btn btn-info" value="search">
                                                <button type="button" class="btn btn-secondary" onclick="window.print()"><i class="fas fa-print"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <table class="table table-striped table-sm display" id="myTable">
                                        @if(request()->query())
                                        <thead>
                                            <tr class="bg-secondary">
                                                <th>SL</th>
                                                <th>Name | Phone | Department</th>
                                                <th>Attendance</th>
                                                <th class="text-right">Paid</th>
                                                <th class="text-right">Due</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php 
                                                $salaryPaid = 0;
                                                $salarySerial = 0;
                                            @endphp
                                            @forelse ($employees as $employee)
                                            @php
                                                $salaryPaid += $employee->salaries->where('month',request('month'))->sum('amount');
                                            @endphp
                                                <tr>
                                                    <td>{{ ++$salarySerial }}</td>
                                                    <td>{{ $employee->name}} | {{$employee->phone}} | {{ $employee->department->name}}</td>
                                                    <td> 
                                                        Present: {{ $employee->attendances->where('month',request('month'))->where('year',request('year'))->where('attendance','p')->count() }} | 
                                                        Absents: {{ $employee->attendances->where('month',request('month'))->where('year',request('year'))->where('attendance','a')->count()}} | 
                                                        Holydays: {{ $employee->attendances->where('month',request('month'))->where('year',request('year'))->where('attendance','h')->count()}} 
                                                    </td>
                                                    <td class="text-right"> {{ number_format($employee->salaries->where('month',request('month'))->sum('amount'),2)}} </td>
                                                    <td class="text-right">{{ number_format($employee->salary -  $employee->salaries->where('month',request('month'))->sum('amount'),2)}}</td>
                                                </tr>             
                                            @empty
                                                <tr>
                                                    <td colspan="5" class="text-center" >No Salary Record found!</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                        <tfoot>
                                            <tr class="text-right bg-secondary">
                                                <td></td>
                                                <td></td>
                                                <td>Total Salary: {{ number_format($employees->sum('salary'),2)}}</td>
                                                <td>Total Paid: {{ number_format($salaryPaid,2) }}</td>
                                                <td>Total Dues: {{ number_format($employees->sum('salary') - $salaryPaid,2)}}</td>
                                            </tr>
                                        </tfoot>
                                        @endif
                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </section><!-- /.content -->
@stop

@section('script')
    <!-- page script -->
    <script type="text/javascript">
        $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").fadeOut(500, function(){
                    $(this).remove();
                });
            }, 1200);

        });

        //search report date wise (function for date picker)
        $(function () {
            $('.datePicker').datepicker({
                maxDate: d,
                autoclose: true,
                format: "yyyy-mm-dd"
            });
        });
    </script>
@stop
