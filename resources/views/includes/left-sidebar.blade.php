<!-- Sidebar -->
<div class="sidebar nano">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset(company()->logo) }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="{{ url('/') }}" class="d-block">{{ company()->name }}</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item {{ isActive(['/']) }}">
                <a href="{{ route('/')}}" class="nav-link {{ isActive(['/']) }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
            {{--Contacts menu starts--}}
            <li class="nav-item has-treeview {{ isActive(['employees','employees/*','customers','customers/*','suppliers','suppliers/*']) }}">
                <a href="#" class="nav-link {{ isActive(['employees','employees/*','customers','customers/*','suppliers','suppliers/*']) }}">
                    <i class="nav-icon far fa-bookmark"></i>
                    <p>Contacts<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('employees.index') }}" class="nav-link {{ isActive(['employees','employees/*']) }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All Employees</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('customers.index') }}" class="nav-link {{ isActive(['customers','customers/*']) }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All Customers</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('suppliers.index') }}" class="nav-link {{ isActive(['suppliers','suppliers/*']) }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All Suppliers</p>
                        </a>
                    </li>
                </ul>
            </li>
            {{--Contacts menu ends--}}

            {{--Attendances menu starts--}}
            <li class="nav-item has-treeview {{ isActive(['attendances','attendances/*']) }}">
                <a href="#" class="nav-link {{ isActive(['attendances','attendances/*']) }}">
                    <i class="nav-icon far fa-handshake"></i>
                    <p>attendances<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('attendances.index') }}" class="nav-link {{ isActive(['attendances','attendances/*']) }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All attendances</p>
                        </a>
                    </li>
                </ul>
            </li>
            {{--Attendances menu ends--}}

            {{--Orders menu starts--}}
            <li class="nav-item has-treeview  {{ isActive(['orders','orders/*']) }}">
                <a href="#" class="nav-link {{ isActive(['orders','orders/*']) }}">
                        <i class="nav-icon fas fa-cart-plus"></i>
                    <p>Orders<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('orders.index') }}" class="nav-link {{ isActive(['orders','orders/*']) }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All Orders</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link {{ isActive(['']) }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All invoices</p>
                        </a>
                    </li>
                </ul>
            </li>
            {{--Orders menu ends--}}

            {{--Reports menu starts--}}
            <li class="nav-item has-treeview {{ isActive(['salaries']) }}">
                <a href="#" class="nav-link {{ isActive(['salaries']) }}">
                <i class="nav-icon fas fa-book"></i>
                    <p>Reports<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('salaries.index') }}" class="nav-link {{ isActive('salaries') }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Monthly Employee Report</p>
                        </a>
                    </li>
                </ul>
            </li>
            {{-- Reports menu ends--}}

            {{--Working Information menu starts--}}
            <li class="nav-item has-treeview {{ isActive(['vehicles','vehicles/*','work_lists','work_lists/*','parts','parts/*']) }}">
                    <a href="#" class="nav-link {{ isActive(['vehicles','vehicles/*','work_lists','work_lists/*','parts','parts/*']) }}">
                        <i class="nav-icon fas fa-fw fa-database"></i>
                        <p>Working Info<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('vehicles.index') }}" class="nav-link {{ isActive(['vehicles','vehicles/*']) }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Vehicle List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('work_lists.index') }}" class="nav-link {{ isActive(['work_lists','work_lists/*']) }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Work List</p>
                            </a>
                         </li>
                         <li class="nav-item">
                            <a href="{{ route('parts.index') }}" class="nav-link {{ isActive(['parts','parts/*']) }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Parts List</p>
                            </a>
                         </li>
                    </ul>
                </li>
            {{-- Working Information menu ends--}}
            {{--Settings menu starts--}}
            <li class="nav-item has-treeview {{ isActive(['company_settings', 'departments','brands','units']) }}">
                    <a href="#" class="nav-link {{ isActive(['company_settings', 'departments','brands','units']) }}">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>Settings<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('company_settings.index') }}" class="nav-link {{ isActive('company_settings') }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Company</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('departments.index') }}" class="nav-link {{ isActive('departments') }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Departments</p>
                            </a>
                         </li>
                         <li class="nav-item">
                            <a href="{{ route('units.index') }}" class="nav-link {{ isActive('units') }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Units</p>
                            </a>
                         </li><li class="nav-item">
                            <a href="{{ route('brands.index') }}" class="nav-link {{ isActive('brands') }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Brands</p>
                            </a>
                         </li>
                    </ul>
                </li>
            {{-- Settings menu ends--}}
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
