<li class="nav-item has-treeview {{ isActive(['principle*','group*','ac*','ledger/groups*','voucher*']) }}">
    <a href="#" class="nav-link {{isActive(['principle*','group*','ac*','ledger/groups*']) }}">
        <i class="nav-icon fas fa-edit"></i>
        <p>Accounts<i class="fas fa-angle-left right"></i> </p>
    </a>

    <ul class="nav nav-treeview" style="background-color: #292929">
        {{-- Laders Start --}}
        <li class="nav-item has-treeview {{ isActive(['ac/ledger','ac/ledger*','ledger/groups','ledger/groups*']) }}">
            <a href="#" class="nav-link {{ isActive('ac/ledger*','ledger/groups') }}">
                <i class="fas fa-tools"></i>
                <p>Ledger<i class="fas fa-angle-left right"></i> </p>
            </a>
            <ul class="nav-treeview nav">

                <li class="nav-item">
                    <a href="{{ url('ledger/groups') }}" class="nav-link {{ isActive('ledger/groups') }}">
                        <p style="margin-left:30px"><i class="far fa-arrow-alt-circle-right"></i> Groups</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('ac/ledger') }}" class="nav-link {{ isActive('ac/ledger') }}">
                        <p style="margin-left:30px"><i class="far fa-arrow-alt-circle-right"></i> Create Ledger</p>
                    </a>
                </li>
            </ul>
        </li>{{-- Ladgers End --}}
    </ul>

    <ul class="nav nav-treeview" style="background-color: #292929">
        {{-- Accounts Journal --}}
        <li class="nav-item has-treeview {{ isActive(['ac/principle*','ac/group*','ac/journal*']) }}">
            <a href="#" class="nav-link {{ isActive('ac/journal*') }}">
                <i class="fas fa-tools"></i>
                <p>Journal <i class="fas fa-angle-left right"></i> </p>
            </a>
            <ul class="nav-treeview nav">
                <li class="nav-item">
                    <a href="{{ url('ac/journal/create') }}" class="nav-link {{ isActive('ac/journal/create') }}">
                        <p style="margin-left:30px"><i class="far fa-arrow-alt-circle-right"></i> Create Journal</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('ac/journal/lists') }}" class="nav-link {{ isActive('ac/journal/lists') }}">
                        <p style="margin-left:30px"><i class="far fa-arrow-alt-circle-right"></i> Journal List</p>
                    </a>
                </li>
            </ul>
        </li>
    </ul>

    {{--Start Balance sheet --}}
    <ul class="nav nav-treeview" style="background-color: #292929">
        {{-- Accounts balance sheet  start --}}
        <li class="nav-item has-treeview {{ isActive(['ac/balance-sheet*']) }}">
            <a href="#" class="nav-link {{ isActive('ac/balance-sheet*') }}">
                <i class="fas fa-tools"></i>
                <p> Balance Sheet <i class="fas fa-angle-left right"></i> </p>
            </a>
            <ul class="nav-treeview nav">
                <li class="nav-item">
                    <a href="{{ url('ac/balance-sheet/lists') }}" class="nav-link {{ isActive('ac/balance-sheet/lists') }}">
                        <p style="margin-left:30px">Balance sheet (index)</p>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    {{--End Balance sheet --}}

    {{--Start Cash Voucher --}}
    <ul class="nav nav-treeview" style="background-color: #292929">
        {{-- Accounts balance sheet  start --}}
        <li class="nav-item has-treeview {{ isActive(['voucher*']) }}">
            <a href="#" class="nav-link {{ isActive('voucher*') }}">
                <i class="fas fa-envelope-open-text"></i>
                <p> Cash Voucher <i class="fas fa-angle-left right"></i></p>
            </a>

            <ul class="nav-treeview nav">
                <li class="nav-item">
                    <a href="{{ url('voucher/cash-receipt/create') }}" class="nav-link {{ isActive('voucher/cash-receipt/create') }}">
                        <p style="margin-left:5px"><i class="far fa-money-bill-alt"></i> Cash Receipt</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('voucher/cash-receipt/report') }}" class="nav-link {{ isActive('voucher/cash-receipt/report') }}">
                        <p style="margin-left:5px; "><i class="far fa-money-bill-alt"></i> Cash Receipt Voucher Report</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('voucher/cash-payment/create') }}" class="nav-link {{ isActive('voucher/cash-payment/create') }}">
                        <p style="margin-left:5px"><i class="fas fa-donate"></i> Cash Payment</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('voucher/cash-payment/report') }}" class="nav-link {{ isActive('voucher/cash-payment/report') }}">
                        <p style="margin-left:5px; "><i class="fas fa-donate"></i> Cash Payment Voucher Report</p>
                    </a>
                </li>
            </ul>

        </li>
    </ul>
    {{--End Cash Voucher --}}

</li>
{{--display menu starts here--}}
<li class="nav-item has-treeview {{ isActive(['display/*']) }}">
    <a href="#" class="nav-link {{ isActive(['display/*']) }}">
        <i class="nav-icon fas fa-edit"></i>
        <p>Display<i class="fas fa-angle-left right"></i> </p>
    </a>
    <ul class="nav nav-treeview" style="background-color: #292929">
        <li class="nav-item">
            <a href="{{ url('display/daybook') }}" class="nav-link {{ isActive('display/daybook') }}">
                <p style="margin-left:30px">Day Book</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('display/receipt_payment') }}" class="nav-link {{ isActive('display/receipt_payment') }}">
                <p style="margin-left:30px">Receipt-Payment</p>
            </a>
        </li>
    </ul>
</li>
{{--display menu ends here--}}

{{--Reports menu starts here--}}
<li class="nav-item has-treeview {{ isActive(['reports/*']) }}">
    <a href="#" class="nav-link {{ isActive(['reports/*']) }}">
        <i class="nav-icon fas fa-edit"></i>
        <p>Reports<i class="fas fa-angle-left right"></i> </p>
    </a>
    <ul class="nav nav-treeview" style="background-color: #292929">
        <li class="nav-item">
            <a href="{{ url('reports/trial_balance') }}" class="nav-link {{ isActive('reports/trial_balance') }}">
                <p style="margin-left:30px">Trial Balance</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('reports/profit_loss') }}" class="nav-link {{ isActive('reports/profit_loss') }}">
                <p style="margin-left:30px">Profit Loss</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('reports/balance_sheet') }}" class="nav-link {{ isActive('reports/balance_sheet') }}">
                <p style="margin-left:30px">Balance Sheet</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ url('reports/ledger') }}" class="nav-link {{ isActive('reports/ledger') }}">
                <p style="margin-left:30px">Ledger Book</p>
            </a>
        </li>
    </ul>
</li>
{{--Reports menu ends here--}}
<li class="nav-item has-treeview {{ isActive(['settings/company*','settings/chart-of-account','settings/*']) }}">
    <a href="#" class="nav-link {{isActive(['settings/company*','settings/chart-of-account','settings/*']) }}">
        <i class="nav-icon fas fa-edit"></i>
        <p>Account Settings<i class="fas fa-angle-left right"></i> </p>
    </a>

    <ul class="nav nav-treeview" style="background-color: #292929">
        <li class="nav-item">
            <a href="{{ url('settings/company/add') }}" class="nav-link {{ isActive('settings/company/add') }}">
                <p style="margin-left:30px">Company Setup</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('settings/chart-of-account') }}" class="nav-link {{ isActive('settings/chart-of-account') }}">
                <p style="margin-left:30px">Chart Of Account</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ url('settings/list-of-groups') }}" class="nav-link {{ isActive('settings/list-of-groups') }}">
                <p style="margin-left:30px">List Of Groups</p>
            </a>
        </li>


        {{-- Accounts info start --}}
{{--        <li class="nav-item has-treeview {{ isActive(['ac/principle*','ac/group*','ac*']) }}">--}}
{{--            <a href="#" class="nav-link {{ isActive('ac/*') }}">--}}
{{--                <i class="fas fa-tools"></i>--}}
{{--                <p> Account info<i class="fas fa-angle-left right"></i> </p>--}}
{{--            </a>--}}
{{--            <ul class="nav-treeview nav">--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="{{ url('ac/principle') }}" class="nav-link {{ isActive('ac/principle') }}">--}}
{{--                        <p style="margin-left:30px">Ac Principle</p>--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--                <li class="nav-item">--}}
{{--                    <a href="{{ url('ac/group') }}" class="nav-link {{ isActive('ac/group') }}">--}}
{{--                        <p style="margin-left:30px">Ac Group</p>--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--                <li class="nav-item">--}}
{{--                    <a href="{{ url('ac/mainhead') }}" class="nav-link {{ isActive('ac/mainhead') }}">--}}
{{--                        <p style="margin-left:30px">Ac Main Head</p>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}
    </ul>
{{--------------}}
    <style>
        .nav-sidebar .nav-link p {
            display: inline-block;
            margin: 0;
            font-size: 14px;
        }
    </style>

