@extends('layouts.fixed')
@section('title', 'Work List')

@section('breadcrumb')
    <li class="breadcrumb-item active">Work Lists</li>
@endsection
@section('content')
    <section class="content mt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary col-md-12">
                        <div class="card-header row">
                            <h3 class="card-title col-md-3">All Work Lists <a title="Click to add new Worklist" class="btn btn-sm " href="{{ route('work_lists.create')}}">
                                <span class="fas fa-plus"></span></a>
                            </h3>
                            <div class="col-md-9">
                                <form class="form-inline float-right" method="GET" action="{{ route('work_lists.index') }}">
                                    <div class="form-group mx-sm-3">
                                    <label for="name" class="sr-only">Name</label>
                                    <input class="form-control" placeholder="Work List Name" name="name" type="text" value="{{ request('name') }}" id="name">
                                    </div>
                                    <div class="form-group mx-sm-3">
                                        <label for="department_id" class="sr-only">Department</label>
                                        <select class="form-control" name="department_id" id="department_id">
                                            <option value="">Select Department</option>
                                            @forelse($departments as $department)
                                            <option {{ request('department_id') == $department->id ? 'selected' : '' }} value="{{$department->id}}">{{ $department->name }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-round"><i class="fa fa-search"></i></button>
                                    @if(request()->query())
                                        <a href="{{ route('work_lists.index') }}" class='btn btn-info btn-round' >All</a>
                                    @endif
                                  </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped">
                                        <thead>
                                            <tr class='bg-secondary'>
                                                <th>#SL</th>
                                                <th>Working List name</th>
                                                <th>Department</th>
                                                <th>800 CC To 1500 CC</th>
                                                <th>1600 CC To 2500 CC</th>
                                                <th>2600 CC+</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        @php $sl = 0 @endphp
                                        @forelse($work_lists as $work_list)
                                        <tr>
                                            <td>{{ ++$sl }}</td>
                                            <td>{{ $work_list->name}}</td>
                                            <td>{{ $work_list->department->name }}</td>
                                            <td class="text-right">{{ number_format($work_list->rate1,2) }}</td>
                                            <td class="text-right">{{ number_format($work_list->rate2,2) }}</td>
                                            <td class="text-right">{{ number_format($work_list->rate3,2) }}</td>
                                            <td class="text-center">
                                                <form action="#" method="post"
                                                    onsubmit=" return confirm('Are you sure to delete?')">
                                                    @csrf
                                                    @method('delete')
                                                    <div class="btn-group">
                                                        {{--  <a href="#" class="btn btn-success"><i class="fa fa-eye"></i></a>  --}}
                                                        {{--  <a href="#" class="btn btn-success" data-toggle="tooltip" data-placement="auto top" title="Show Status"><i class="fa fa-list"></i></a>  --}}
                                                        <a href="{{ route('work_lists.edit',1) }}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                                        {{--  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>  --}}
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                        @empty

                                        @endforelse
                                            
                                        </tbody>
                                    </table>
                                    {{-- {{ $products->appends(request()->query())->links() }} --}}
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@endsection