@extends('layouts.fixed')
@section('title', 'Work List')
@section('breadcrumb')
    <li class="breadcrumb-item active">Work List Update</li>
@endsection
@section('content')
<section class="content mt-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- form start -->
                <form role="form" action="{{ route('work_lists.update', $workList->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Work List Update Form</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- general form elements -->
                                    <div class="box box-info">
                                        <div class="box-header with-border">
                                            <h3 class="box-title"></h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <div class="box-body">
                                           <div class="form-group row">
                                                    <label for="name" class="col-sm-2 control-label">Name</label>
                                
                                                    <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="name" name="name" value="{{old('name') ?? $workList->name }}" placeholder="Name">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="cc" class="col-sm-2 control-label">Department</label>
                                                    <div class="col-sm-10">
                                                        <select class="form-control" name="department_id">
                                                            <option value=''>select Department</option>
                                                            @forelse($departments as $department)
                                                                <option {{ $workList->department_id == $department->id ? 'selected' : ''}} value="{{$department->id}}">{{ $department->name }}</option>
                                                            @empty

                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="no" class="col-sm-2 control-label">Rate 1</label>
                                
                                                    <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="" name="rate1" value="{{ $workList->rate1 }}" placeholder="Enter Rate for 800CC to 1500 CC">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="no" class="col-sm-2 control-label">Rate 2</label>
                                
                                                    <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="" name="rate2" value="{{ $workList->rate2 }}" placeholder="Enter Rate for 1600 to 2500 CC">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="no" class="col-sm-2 control-label">Rate 3</label>
                                
                                                    <div class="col-sm-10">
                                                    <input type="text" class="form-control" id=""  name="rate3" value="{{ $workList->rate3 }}" placeholder="Enter Rate for 2600 CC +">
                                                    </div>
                                                </div> 
                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer"></div>
                                        <!-- /.box-footer -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ route('work_lists.index') }}" class="btn btn-danger">Cancel</a>
                            <a href="{{ route('work_lists.index') }}" class="btn btn-primary">Reset Form</a>
                            <button type="submit" class="btn btn-success float-right">Update</button>
                        </div>
                    </div><!-- /.card -->
                </form>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('script')
<script>

</script>
@endsection
