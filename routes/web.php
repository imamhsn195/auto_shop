<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['reset' => false, 'register' => false]);

Route::view('test', 'test');

Route::group(['middleware'=>'auth'], function (){

    Route::get('/', 'DashboardController@index')->name('/');

    /*    Resource Controllers starts     */

    Route::resource('products', 'ProductController');

    Route::resource('product_categories', 'ProductCategoryController');

    Route::resource('expenses', 'ExpenseController');

    Route::resource('expense_categories', 'ExpenseCategoryController');

    Route::resource('contractors','ContractorController');

    Route::resource('contractor_types', 'ContractorTypeController');

    Route::resource('measurement_units', 'MeasurementUnitController');

    Route::resource('product_types', 'ProductTypeController');

    Route::resource('product_models', 'ProductModelController');

    Route::resource('company_settings','CompanySettingController');

    Route::resource('vehicles','VehicleController');

    Route::resource('orders','OrderController');
    
	Route::resource('departments','DepartmentController');

	Route::resource('work_lists','WorkListController');

	Route::resource('orderdetails','OrderDetailController');

	Route::resource('customers','CustomerController');

	Route::resource('suppliers', 'SupplierController');

    Route::resource('employees', 'EmployeeController');
    
    Route::resource('attendances', 'AttendanceController');
    /** Salary Routes starts */

    Route::resource('salaries', 'SalaryController');

    Route::get('monthly_salary_dues', 'SalaryController@monthly_salary_dues')->name('monthly_salary_dues');
    
    /** Salary Routes starts */
    Route::resource('parts', 'PartController');

    Route::resource('orders','OrderController');

    Route::resource('materials', 'MaterialController');

    Route::resource('material_types', 'MaterialTypeController');

    Route::resource('material_stocks', 'MaterialStockController');

    Route::resource('brands', 'BrandController');

    Route::resource('units', 'UnitController');

/*    Resource Controllers ends     */


/*    Reports Routes starts     */

    Route::get('report/material', 'ReportController@Material')->name('materials_report');

    Route::get('report/contractor', 'ReportController@Contractor')->name('contractors_report');

    Route::get('delivered_products', 'ReportController@delivered_products')->name('delivered_products');

    Route::get('report/material_product', 'ReportController@MaterialProduct')->name('material_product');


/*    Reports Routes ends     */

/** Material Assignment Routes starts */

    Route::get('assign_material_history', 'AssignMaterialController@index')->name('assign_material_history');

    Route::get('assign_material', 'AssignMaterialController@create')->name('assign_material');

    Route::post('assign_material', 'AssignMaterialController@assignMaterialToProduct')->name('assign_material');

/** Material Assignment Routes ends */

	Route::get('orderDetailupdate/{id}','OrderDetailController@orderDetailupdate');

	Route::get('customer/{phone}','CustomerController@index');
});

/** Cache Clear Routes Starts */

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    session()->flash('success','Cache Cleared successfull');
    return redirect()->route('/');
});
/** Cache Clear Routes Ends */
