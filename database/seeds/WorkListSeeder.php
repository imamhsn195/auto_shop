<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //worklist seeder 

        //Engine System department_id
        DB::table('work_lists')->insert([
            'name'=>'Engine Replace (New Setting)', 
            'department_id'=>'1', 
            'rate1'=>'5000', 
            'rate2'=>'7000', 
            'rate3'=>'8000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Replace with Gear Box', 
            'department_id'=>'1', 
            'rate1'=>'7000', 
            'rate2'=>'10000', 
            'rate3'=>'12000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Engine Overhauling (Major)', 
            'department_id'=>'1', 
            'rate1'=>'8000', 
            'rate2'=>'10000', 
            'rate3'=>'12000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Top Overhuling', 
            'department_id'=>'1', 
            'rate1'=>'4000', 
            'rate2'=>'5000', 
            'rate3'=>'6000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Valve Timing Belt Replacement', 
            'department_id'=>'1', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Valve Timing Chain Replacement', 
            'department_id'=>'1', 
            'rate1'=>'8000', 
            'rate2'=>'1000', 
            'rate3'=>'12000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Group Belt Replace (Each)', 
            'department_id'=>'1', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Timing Cover Oil Seal Replacement', 
            'department_id'=>'1', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Main Oil Seal Replacement', 
            'department_id'=>'1', 
            'rate1'=>'3000', 
            'rate2'=>'4000', 
            'rate3'=>'5000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine mounting Replace(Each)', 
            'department_id'=>'1', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Head Gasket Replace(Each)', 
            'department_id'=>'1', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Tappet Cover Seal Replace', 
            'department_id'=>'1', 
            'rate1'=>'400', 
            'rate2'=>'300', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Plug Guide Oil Seal Replace', 
            'department_id'=>'1', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Accelerator Cable Replace', 
            'department_id'=>'1', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Speeometer Cable Replace(Each)', 
            'department_id'=>'1', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Belt Adjuster /Tensioner Replace', 
            'department_id'=>'1', 
            'rate1'=>'400', 
            'rate2'=>'600', 
            'rate3'=>'700'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Pulley Replace', 
            'department_id'=>'1', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Punching', 
            'department_id'=>'1', 
            'rate1'=>'15000', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Engine Cleaning', 
            'department_id'=>'1', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'700'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Turbo Charger System Works', 
            'department_id'=>'1', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);

        // Lubrication System department_id
        DB::table('work_lists')->insert([
            'name'=>'Oil Sump Replace', 
            'department_id'=>'2', 
            'rate1'=>'600', 
            'rate2'=>'600', 
            'rate3'=>'600'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Oil Sump Repair', 
            'department_id'=>'2', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Oil Sump Seal Replace', 
            'department_id'=>'2', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Oil Pump Replace', 
            'department_id'=>'2', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Oil Strainer Servicing/Net/Filter', 
            'department_id'=>'2', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Oil Pressure Switch Replace', 
            'department_id'=>'2', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);

        // Electrical System
        DB::table('work_lists')->insert([
            'name'=>'Fuse Box Replace', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'1000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Fuse Box (Power Source) Repair', 
            'department_id'=>'3', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Ignition Switch Repair', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Ignition Switch Replace', 
            'department_id'=>'3', 
            'rate1'=>'600', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CombiN/Ation Switch Repair', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CombiN/Ation Switch Replace', 
            'department_id'=>'3', 
            'rate1'=>'1200', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fog light Replace(Each)', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'500', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fog light Setup', 
            'department_id'=>'3', 
            'rate1'=>'600', 
            'rate2'=>'600', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fog light switch Replace', 
            'department_id'=>'3', 
            'rate1'=>'400', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Back light Replace (Each)', 
            'department_id'=>'3', 
            'rate1'=>'400', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wiper & Washer System works', 
            'department_id'=>'3', 
            'rate1'=>'400', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wiper blade Replace', 
            'department_id'=>'3', 
            'rate1'=>'700', 
            'rate2'=>'700', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wiper Switch Repair', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wiper System Cheak -Up & Necessary Works', 
            'department_id'=>'3', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door Master Switch Replace', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'1200', 
            'rate3'=>'1400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door Master Switch Repair', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'1200', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door lock servicing & Adjustment', 
            'department_id'=>'3', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door lock Repalce', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door Handle Replace', 
            'department_id'=>'3', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door Glass switch Repair', 
            'department_id'=>'3', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door Glass switch Replace', 
            'department_id'=>'3', 
            'rate1'=>'200', 
            'rate2'=>'300', 
            'rate3'=>'300'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door Glass motor Replace', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door Glass Machine Repair', 
            'department_id'=>'3', 
            'rate1'=>'600', 
            'rate2'=>'700', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Door Glass Machine servicing', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'700'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Vehicle Speed Sensor Replace', 
            'department_id'=>'3', 
            'rate1'=>'600', 
            'rate2'=>'700', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Vehicle Speed Sensor Repair', 
            'department_id'=>'3', 
            'rate1'=>'600', 
            'rate2'=>'700', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CombiN/Ation Meter Servicing & Replace', 
            'department_id'=>'3', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CombiN/Ation Switch Repair', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CombiN/Ation Switch Replace', 
            'department_id'=>'3', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'ABS Check & repair(Minor)', 
            'department_id'=>'3', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'ABS Check & repair(Major)', 
            'department_id'=>'3', 
            'rate1'=>'2500', 
            'rate2'=>'3000', 
            'rate3'=>'3000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'SRS Check & Repair(Minor)', 
            'department_id'=>'3', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'SRS Check & Repair(Major)', 
            'department_id'=>'3', 
            'rate1'=>'2000', 
            'rate2'=>'2300', 
            'rate3'=>'3000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Ignition Replace', 
            'department_id'=>'3', 
            'rate1'=>'300', 
            'rate2'=>'300', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'High Tension lead Replace(Flug Cable)', 
            'department_id'=>'3', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Distributor (With Ignition Coil) Repair (Minor)', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Distributor (With Ignition Coil) Repair (Major)', 
            'department_id'=>'3', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Distributor Cap Replace', 
            'department_id'=>'3', 
            'rate1'=>'300', 
            'rate2'=>'300', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Self Starter Motor Opening & refitting', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Self Starter Overhauling', 
            'department_id'=>'3', 
            'rate1'=>'1500', 
            'rate2'=>'1500', 
            'rate3'=>'2000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AlterN/Ator Overhauling/ Repair', 
            'department_id'=>'3', 
            'rate1'=>'1500', 
            'rate2'=>'2000', 
            'rate3'=>'2200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AlterN/Ator Replace', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AlterN/Ator Electric line check-up & repair', 
            'department_id'=>'3', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AlterN/Ator opening & refitting', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Horn replace', 
            'department_id'=>'3', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Horn tuning', 
            'department_id'=>'3', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Horn wiring Major', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'1200', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Horn wiring Minor', 
            'department_id'=>'3', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Airflow Sensor Servicing', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Glass Motor Change (Each)', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'700'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wiring Check-up &  Repair for light (Minor)', 
            'department_id'=>'3', 
            'rate1'=>'400', 
            'rate2'=>'600', 
            'rate3'=>'700'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wiper Motor Servicing', 
            'department_id'=>'3', 
            'rate1'=>'600', 
            'rate2'=>'700', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Electric line Check-up & Report', 
            'department_id'=>'3', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Ignition Coil Replace', 
            'department_id'=>'3', 
            'rate1'=>'200', 
            'rate2'=>'300', 
            'rate3'=>'300'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Distributor Replace', 
            'department_id'=>'3', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Distributor Servicing', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CB Point/Condeser Replace', 
            'department_id'=>'3', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'High Tension lead Replace', 
            'department_id'=>'3', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Ignition System Check-Up & Report', 
            'department_id'=>'3', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        // Clutch System
        DB::table('work_lists')->insert([
            'name'=>'Clutch/Pressure Plate Replace', 
            'department_id'=>'4', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Clutch Master Cylinder Replace', 
            'department_id'=>'4', 
            'rate1'=>'600', 
            'rate2'=>'700', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Clutch Sleeve Cylinder Replace', 
            'department_id'=>'4', 
            'rate1'=>'400', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Clutch Pedal Adjustment', 
            'department_id'=>'4', 
            'rate1'=>'200', 
            'rate2'=>'300', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Clutch Cable Adjustment', 
            'department_id'=>'4', 
            'rate1'=>'200', 
            'rate2'=>'300', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'4 Bleeding', 
            'department_id'=>'4', 
            'rate1'=>'300', 
            'rate2'=>'300', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'4 Overhauling', 
            'department_id'=>'4', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
// Engine System & Electrical Check-Up

        DB::table('work_lists')->insert([
            'name'=>'EFI System Check-Up & Necessary Wiring Repair (Major)', 
            'department_id'=>'5', 
            'rate1'=>'6000', 
            'rate2'=>'7000', 
            'rate3'=>'8000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'EFI System Check-Up & Necessary Wiring Repair (Minor)', 
            'department_id'=>'5', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'4000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Computerized Engine AN/Alysis or Engine Scanning (Check-Up only)', 
            'department_id'=>'5', 
            'rate1'=>'1500', 
            'rate2'=>'1500', 
            'rate3'=>'2000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Computerized Engine AN/Alysis or Engine Scanning & Tune Up', 
            'department_id'=>'5', 
            'rate1'=>'1500', 
            'rate2'=>'1500', 
            'rate3'=>'2000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'EFI Fault Code Assessing & Erasing (By Scanner)', 
            'department_id'=>'5', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'EFI Fault Code Assessing & Erasing (Manually)', 
            'department_id'=>'5', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'ECU Box Repair', 
            'department_id'=>'5', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'ECU Box Replace', 
            'department_id'=>'5', 
            'rate1'=>'1000', 
            'rate2'=>'1500', 
            'rate3'=>'2000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Auto Tappet Clearance Adjustment', 
            'department_id'=>'5', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'2000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Valve / Tappet Clearance Adjustment (Manua typel)', 
            'department_id'=>'5', 
            'rate1'=>'1200', 
            'rate2'=>'1300', 
            'rate3'=>'1500'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Intake Manifold Wash', 
            'department_id'=>'5', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Throttle Body Replace', 
            'department_id'=>'5', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Throttle Body Service', 
            'department_id'=>'5', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'700'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Engine Tuning', 
            'department_id'=>'5', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Engine Compression Test & Report', 
            'department_id'=>'5', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'General Check-Up', 
            'department_id'=>'5', 
            'rate1'=>'200', 
            'rate2'=>'250', 
            'rate3'=>'300'
        ]);
        // Denting & Painting
        
        DB::table('work_lists')->insert([
            'name'=>'Accidental Dent', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null   
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Body Part Adjustment', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Decorative Work', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Dent & Paint', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Exhaust System Work', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Noise Removal', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Paint (Contractual)', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Dent (Contractual)', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Dumper Replace', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Seat Adjustment', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Touch Up Dent & Paint', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Welding Works', 
            'department_id'=>'6', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
    // Wheel System
        DB::table('work_lists')->insert([
            'name'=>'Computerized Wheel Alignment', 
            'department_id'=>'7', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null        
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Computerized Camber Adjustment', 
            'department_id'=>'7', 
            'rate1'=>'500', 
            'rate2'=>'700', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Computerized Wheel Balancing Replace (Each)', 
            'department_id'=>'7', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null        
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wheel Hubs Bearing Replace (Each)', 
            'department_id'=>'7', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wheel Hubs Seal Replace (Each)', 
            'department_id'=>'7', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wheel Hubs Bush Replace (Each)', 
            'department_id'=>'7', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wheel Hubs Replace (Each)', 
            'department_id'=>'7', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wheel Rotation', 
            'department_id'=>'7', 
            'rate1'=>'300', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Weights (Clip Type Per Gram)', 
            'department_id'=>'7', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Weights (Adhesive Type Per Gram)', 
            'department_id'=>'7', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
 // Cooling System
        DB::table('work_lists')->insert([
            'name'=>'Radiator Replace', 
            'department_id'=>'8', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'700'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Radiator Repair', 
            'department_id'=>'8', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Water Pump Replace', 
            'department_id'=>'8', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Water Jacket Replace', 
            'department_id'=>'8', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Hose Pipe Replace(Each)', 
            'department_id'=>'8', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Themostar Switch Replace', 
            'department_id'=>'8', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Themostar Valve Replace', 
            'department_id'=>'8', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Cooling Fan Replace', 
            'department_id'=>'8', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Temperature Switch Replace', 
            'department_id'=>'8', 
            'rate1'=>'200', 
            'rate2'=>'300', 
            'rate3'=>'400'
        ]);
 // Transmission System
        DB::table('work_lists')->insert([
            'name'=>'Gearbox Overhauling (Manual)', 
            'department_id'=>'9', 
            'rate1'=>'5000', 
            'rate2'=>'6000', 
            'rate3'=>'8000'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Gearbox Overhauling (Auto)', 
            'department_id'=>'9', 
            'rate1'=>'10000', 
            'rate2'=>'12000', 
            'rate3'=>'15000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Gearbox Replace (Manual)', 
            'department_id'=>'9', 
            'rate1'=>'3000', 
            'rate2'=>'4000', 
            'rate3'=>'5000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Gearbox Repalce (Auto)', 
            'department_id'=>'9', 
            'rate1'=>'4000', 
            'rate2'=>'5000', 
            'rate3'=>'6000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Gearbox Oil Seal Replace', 
            'department_id'=>'9', 
            'rate1'=>'4500', 
            'rate2'=>'5500', 
            'rate3'=>'6500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'ATF-Auto Transmission Fluid Flush & Replace (By Machine)', 
            'department_id'=>'9', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'ATF-Auto Transmission Fluid Flush & Replace (By Manual)', 
            'department_id'=>'9', 
            'rate1'=>'500', 
            'rate2'=>'700', 
            'rate3'=>'900'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'ATF-Auto Transmission Fluid Replace', 
            'department_id'=>'9', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CVTF-Continusly Variable Transmission Fluid Flush & Replace', 
            'department_id'=>'9', 
            'rate1'=>'500', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CVFT Learning', 
            'department_id'=>'9', 
            'rate1'=>'800', 
            'rate2'=>'800', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Differential Assembly Replace', 
            'department_id'=>'9', 
            'rate1'=>'2500', 
            'rate2'=>'3000', 
            'rate3'=>'3500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Differential Overhauling', 
            'department_id'=>'9', 
            'rate1'=>'3000', 
            'rate2'=>'3500', 
            'rate3'=>'4000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Front Axle Shaft Replace', 
            'department_id'=>'9', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Rear Axle Shaft Replace', 
            'department_id'=>'9', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Rear Axle Shaft Oil Seal Replace', 
            'department_id'=>'9', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Propeller Shaft Mounting Replace', 
            'department_id'=>'9', 
            'rate1'=>'1500', 
            'rate2'=>'1500', 
            'rate3'=>'2000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Propeller Shaft Bearing Replace', 
            'department_id'=>'9', 
            'rate1'=>'800', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Auto Gear Cable Adjusting', 
            'department_id'=>'9', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'9 Check-Up & Report***', 
            'department_id'=>'9', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
// Fuel System
        DB::table('work_lists')->insert([
            'name'=>'Carburetor Overhauling', 
            'department_id'=>'10', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Fuel Pump Replace (In-Line)', 
            'department_id'=>'10', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fuel Pump Replace (In-Tank)', 
            'department_id'=>'10', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fuel Tank Open/fitting /wash', 
            'department_id'=>'10', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fuel Filter Replace (In Tank)', 
            'department_id'=>'10', 
            'rate1'=>'600', 
            'rate2'=>'900', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fuel Filter Replace (In Line)', 
            'department_id'=>'10', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Injector Ultrasonic Cleaning & Servicing', 
            'department_id'=>'10', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Injector Replace/Open/Fitting', 
            'department_id'=>'10', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Injector Seal Replace', 
            'department_id'=>'10', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fuel Meter Service', 
            'department_id'=>'10', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'10 Servicing', 
            'department_id'=>'10', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);
// AC System
        DB::table('work_lists')->insert([
            'name'=>'AC Gas Charge (406)', 
            'department_id'=>'11', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AC Gas Charge Dual (406)', 
            'department_id'=>'11', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AC Gas Charge (134a)', 
            'department_id'=>'11', 
            'rate1'=>'2500', 
            'rate2'=>'3000', 
            'rate3'=>'3500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AC Gas Charge Dual (134a)', 
            'department_id'=>'11', 
            'rate1'=>'4500', 
            'rate2'=>'5000', 
            'rate3'=>'5500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AC Compressor Replace', 
            'department_id'=>'11', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AC Condenser Replace', 
            'department_id'=>'11', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AC Compressor Bearing Replace', 
            'department_id'=>'11', 
            'rate1'=>'700', 
            'rate2'=>'900', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AC Pull Vacuum Replace', 
            'department_id'=>'11', 
            'rate1'=>'450', 
            'rate2'=>'550', 
            'rate3'=>'650'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AC Push Vacuum Replace', 
            'department_id'=>'11', 
            'rate1'=>'450', 
            'rate2'=>'550', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Blower Motor Replace', 
            'department_id'=>'11', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'AC Adjuster Bearing Replace', 
            'department_id'=>'11', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'11 Replace (New Setting)', 
            'department_id'=>'11', 
            'rate1'=>'5000', 
            'rate2'=>'6000', 
            'rate3'=>'7000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'11 Works (With Dashbord)', 
            'department_id'=>'11', 
            'rate1'=>'5000', 
            'rate2'=>'6000', 
            'rate3'=>'7000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'11 Works (Without Dashbord)', 
            'department_id'=>'11', 
            'rate1'=>'2500', 
            'rate2'=>'3000', 
            'rate3'=>'3500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Full 11 Check-Up & Report', 
            'department_id'=>'11', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Full 11 Check-Up & Report (With Dashbord)', 
            'department_id'=>'11', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);
// CNG System
        DB::table('work_lists')->insert([
            'name'=>'CNG Tuning', 
            'department_id'=>'12', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'CNG Kit Servicing', 
            'department_id'=>'12', 
            'rate1'=>'1500', 
            'rate2'=>'2000', 
            'rate3'=>'2000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Cylinder Servicing', 
            'department_id'=>'12', 
            'rate1'=>'1500', 
            'rate2'=>'2000', 
            'rate3'=>'2000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Kit Replace', 
            'department_id'=>'12', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Kit Repair', 
            'department_id'=>'12', 
            'rate1'=>'1600', 
            'rate2'=>'2000', 
            'rate3'=>'2000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Cylinder Replace', 
            'department_id'=>'12', 
            'rate1'=>'1200', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Cylinder Re-Test', 
            'department_id'=>'12', 
            'rate1'=>'3000', 
            'rate2'=>'3000', 
            'rate3'=>'3000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Shut Up Valve Replace', 
            'department_id'=>'12', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Filling Vlave Replace', 
            'department_id'=>'12', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Mixer Replace', 
            'department_id'=>'12', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Mixer Servicing', 
            'department_id'=>'12', 
            'rate1'=>'300', 
            'rate2'=>'300', 
            'rate3'=>'300'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Switch Replace', 
            'department_id'=>'12', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Pressure Meter Replace', 
            'department_id'=>'12', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'High Pressure Pipe Replace', 
            'department_id'=>'12', 
            'rate1'=>'800', 
            'rate2'=>'900', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Law Pressure Pipe Replace', 
            'department_id'=>'12', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Wiring Works (Minor)', 
            'department_id'=>'12', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CNG Wiring Works (Major)', 
            'department_id'=>'12', 
            'rate1'=>'4000', 
            'rate2'=>'5000', 
            'rate3'=>'6000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'TAP-Timing Advance Processor Replace', 
            'department_id'=>'12', 
            'rate1'=>'800', 
            'rate2'=>'800', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Timing Relay Replace', 
            'department_id'=>'12', 
            'rate1'=>'500', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fuel Pump Controller Replace', 
            'department_id'=>'12', 
            'rate1'=>'400', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'12 Removal', 
            'department_id'=>'12', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Full 12 Installation', 
            'department_id'=>'12', 
            'rate1'=>null, 
            'rate2'=>null, 
            'rate3'=>null
        ]);

// Suspension System
        DB::table('work_lists')->insert([
            'name'=>'Shock Absorber Replace (Pair)', 
            'department_id'=>'13', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Shock Absorber Mounting Replace (Pair)', 
            'department_id'=>'13', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Cross Member Assembly Replace', 
            'department_id'=>'13', 
            'rate1'=>'2000', 
            'rate2'=>'3000', 
            'rate3'=>'3500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Complete Arm Replace', 
            'department_id'=>'13', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Arm Bush Replace', 
            'department_id'=>'13', 
            'rate1'=>'600', 
            'rate2'=>'700', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Ball Joint Replace', 
            'department_id'=>'13', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Link Rod Replace', 
            'department_id'=>'13', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Stabilizer Bar Replace', 
            'department_id'=>'13', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Stabilizer Bush Replace', 
            'department_id'=>'13', 
            'rate1'=>'400', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Leaf Spring Replace', 
            'department_id'=>'13', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Coil Spring Replace', 
            'department_id'=>'13', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'High Spacer Installation  (Aluminium)(Pair)', 
            'department_id'=>'13', 
            'rate1'=>'4000', 
            'rate2'=>'4500', 
            'rate3'=>'5000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'High Spacer Installation  (Rubber)(Pair)', 
            'department_id'=>'13', 
            'rate1'=>'3000', 
            'rate2'=>'3000', 
            'rate3'=>'3000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Strut Bar Replace', 
            'department_id'=>'13', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Torsion Bar Replace', 
            'department_id'=>'13', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Torsion Bar Bush Replace', 
            'department_id'=>'13', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'13 Repair', 
            'department_id'=>'13', 
            'rate1'=>'4000', 
            'rate2'=>'7000', 
            'rate3'=>'12000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'13 Overhauling', 
            'department_id'=>'13', 
            'rate1'=>'3000', 
            'rate2'=>'3500', 
            'rate3'=>'4500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'13 Check-Up & Report', 
            'department_id'=>'13', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);

       // Brake System
        DB::table('work_lists')->insert([
            'name'=>'Brake Shoe Replace (Rear)', 
            'department_id'=>'14', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Brake Pads Replace (fornt)', 
            'department_id'=>'14', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'14 Servicing', 
            'department_id'=>'14', 
            'rate1'=>'600', 
            'rate2'=>'700', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'ABS Module Replace', 
            'department_id'=>'14', 
            'rate1'=>'1500', 
            'rate2'=>'2000', 
            'rate3'=>'2500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Master Cylinder Replace', 
            'department_id'=>'14', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Master Cylinder Overhauling', 
            'department_id'=>'14', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wheel Cylinder Replace (Pair)', 
            'department_id'=>'14', 
            'rate1'=>'400', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Wheel Cylinder Servicing (Pair)', 
            'department_id'=>'14', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Caliper Kit Replace', 
            'department_id'=>'14', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Fornt Wheel Cylinder overhauling (Each)', 
            'department_id'=>'14', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'900'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Brake Hose Pipe Replace', 
            'department_id'=>'14', 
            'rate1'=>'300', 
            'rate2'=>'500', 
            'rate3'=>'600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'14 Bleeding', 
            'department_id'=>'14', 
            'rate1'=>'300', 
            'rate2'=>'500', 
            'rate3'=>'700'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Brake Booster Replace', 
            'department_id'=>'14', 
            'rate1'=>'1000', 
            'rate2'=>'1200', 
            'rate3'=>'1500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Brake Booster Check Valve Replace', 
            'department_id'=>'14', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'400'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Parking Brake Cable Replace', 
            'department_id'=>'14', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Brake Cable Junction Replace/Presser pipe', 
            'department_id'=>'14', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Brake Disk/Drum Re-pharse (Each)', 
            'department_id'=>'14', 
            'rate1'=>'200', 
            'rate2'=>'200', 
            'rate3'=>'200'
        ]);
// Steering System
        DB::table('work_lists')->insert([
            'name'=>'Rack End Replace (Each)', 
            'department_id'=>'15', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        
        DB::table('work_lists')->insert([
            'name'=>'Tie Rod End Replace', 
            'department_id'=>'15', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Steering Column Replace', 
            'department_id'=>'15', 
            'rate1'=>'1000', 
            'rate2'=>'1400', 
            'rate3'=>'1600'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Steering Column Bush Works', 
            'department_id'=>'15', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Power Steering Pump Replace', 
            'department_id'=>'15', 
            'rate1'=>'800', 
            'rate2'=>'1000', 
            'rate3'=>'1200'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Power Steering Pipe Replace', 
            'department_id'=>'15', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Steering Box Bush Works', 
            'department_id'=>'15', 
            'rate1'=>'2000', 
            'rate2'=>'2500', 
            'rate3'=>'3000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Steering Cross Replace', 
            'department_id'=>'15', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Steering Cross Bearing Replace', 
            'department_id'=>'15', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CV Joint Replace (Each)', 
            'department_id'=>'15', 
            'rate1'=>'500', 
            'rate2'=>'600', 
            'rate3'=>'800'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'CV Joint Boot Cover Replace (Each)', 
            'department_id'=>'15', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Steering Boot Cover Replace (Each)', 
            'department_id'=>'15', 
            'rate1'=>'300', 
            'rate2'=>'400', 
            'rate3'=>'500'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'15 Flushing', 
            'department_id'=>'15', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Power Steering Box Replace', 
            'department_id'=>'15', 
            'rate1'=>'1200', 
            'rate2'=>'1500', 
            'rate3'=>'2000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'Power Steering Box Repair', 
            'department_id'=>'15', 
            'rate1'=>'3000', 
            'rate2'=>'6000', 
            'rate3'=>'8000'
        ]);
        DB::table('work_lists')->insert([
            'name'=>'15 Check-Up & Report', 
            'department_id'=>'15', 
            'rate1'=>'600', 
            'rate2'=>'800', 
            'rate3'=>'1000'
        ]);

    }
}
