<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name'=>'Engine System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Lubrication System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Electrical System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Clutch System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Electrical Check-Up'
        ]);
        DB::table('departments')->insert([
            'name'=>'Denting & Painting'
        ]);
        DB::table('departments')->insert([
            'name'=>'Wheel System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Cooling System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Transmission System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Fuel System'
        ]);
        DB::table('departments')->insert([
            'name'=>'AC System'
        ]);
        DB::table('departments')->insert([
            'name'=>'CNG System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Suspension System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Brake System'
        ]);
        DB::table('departments')->insert([
            'name'=>'Steering System'
        ]);
    }
}