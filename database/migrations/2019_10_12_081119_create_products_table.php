<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->bigInteger('model_id');
            $table->bigInteger('category_id');
            $table->bigInteger('measurement_unit_id');
            $table->bigInteger('plainer_id')->nullable();
            $table->boolean('plainer_status')->default(0);
            $table->bigInteger('lacquer_id')->nullable();
            $table->boolean('lacquer_status')->default(0);
            $table->date('order_date')->nullable();
            $table->date('delivery_date')->nullable();
            $table->boolean('delivery_status')->default(0);
            $table->text('note')->nullable();
            $table->integer('quantity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
