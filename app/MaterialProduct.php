<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialProduct extends Model
{
    protected $fillable = ['product_id','quantity','contractor_id'];
    protected $table = 'material_product';

    protected $with = ['product','contractor','material'];
    
    public function product()
    {   
        return $this->belongsTo('App\Product');
    }

    
    public function contractor()
    {
        return $this->belongsTo('App\Contractor');
    }

    
    public function material()
    {
        return $this->belongsTo('App\Material');
    }
    
    
    
}
