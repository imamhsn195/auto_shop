<?php
use App\CompanySetting;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use App\Material;
/**
 * @return \Illuminate\Config\Repository|mixed
 */
function getPaginationItems()
{
    return config('settings.pagination');
}

/**
 * @return mixed
 */
function company(){
    return CompanySetting::latest()->first();
}

function materials()
{
    return Material::all();
}

/**
 * @param $request
 * @param $field_name
 * @param $folder
 * @return bool|string
 */
function image_upload($request, $field_name , $folder, $store_name = '')
{
    $image = $request->file($field_name);
    $slug = str_slug($request->name ?? '');
    $location = 'storage/'.$folder;
    if(isset($image)){
        $currentDate = Carbon::now()->toDateString();
        if($store_name == ''){
            $image_name = $slug .'_'. $currentDate .'_'. uniqid() .'.'. $image->getClientOriginalExtension();
        }else{
            $image_name = $store_name;
        }
        if(!file_exists( $location )){
            mkdir( $location , 0777, true);
        }
        $image->move( $location ,  $image_name);
    }else{
        return false;
    }
    return  $location.'/'.$image_name;
}


/**
 * @param $file_with_location
 * @return bool
 */
function image_delete($file_with_location){
    if(file_exists($file_with_location)){
        return unlink($file_with_location);
    }
}

/**
 * @param $path
 * @param string $active
 * @return string
 */
function isActive($path, $active = 'active menu-open'){
    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

/**
 * @param $string
 * @return bool|string
 */
function str_slug($string){
    if(!is_string($string)) return false;
    return implode('_',explode(' ', $string));
}

/**
 * @param $date
 * @param string $format
 * @return string
 */
function datetoread($date, $format = 'd/m/Y')
{
    return Carbon::parse($date)->format($format);
}

/**
 * @param string $prefix
 * @return string
 */
function generateOrderNo($prefix  = 'order')
{
    // Today's date in Ymd format, e.g. 20190704
    $today = Carbon::today()->format('Ymd');

    // Last Order created today
    $lastOrder = Order::whereDate('created_at', Carbon::today())->orderByDesc('id')->first();

    // Strip last three characters of the order no if any order job founded, else start from 1
    $lastOrderIndexOfToday = $lastOrder ? ((int) substr($lastOrder->order_no, -3)) : 0;

    // Fill the new index with preceding zeroes
    $currentOrderIndex = str_pad(++$lastOrderIndexOfToday, 3, "0", STR_PAD_LEFT);

    // make and return the new order number
    return $prefix . '/' . $today . '' . $currentOrderIndex;
}
function months(){
    return ['January','February','March','April','May','June','July','August','September','October','Nobember','December'];
}

function getMonthName(int $month_index)
{
    $months = months();
    return $months[$month_index];
}
function years(){
    return [2019,2020,2021];
}