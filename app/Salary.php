<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $fillable = ['month','year', 'employee_id','amount'];
    
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
    
}
