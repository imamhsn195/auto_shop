<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkList extends Model
{
    protected $fillable = ['name','department_id','rate1','rate2','rate3'];
   
    public function setRateupto1_5kccAttribute($value){
        $this->attributes['rateupto1_5kcc'] = (double)$value;
    }

    // A worklist has many job Details
    function workdetails()
    {
        return $this->hasMany('App\JobDetail');
    }
    
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    
    public function orderDetail()
    {
        return $this->hasMany('App\OrderDetail','work_list_id');
    }
    
    public function department()
    {
        return $this->belongsTo('App\Department');
    }
}
