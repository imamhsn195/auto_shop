<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeasurementUnit extends Model
{
    protected $fillable = ['name','description'];

    public static $insertRoles = [
        'name'=>'required',
        'description' => 'nullable'
    ];
    public static $updateRoles = [
        'name'=>'required',
        'description' => 'nullable'
    ];
}
