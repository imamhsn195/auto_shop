<?php

namespace App;

use App\Material;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected  $fillable = ['name','product_img','category_id','model_id','plainer_id','plainer_status','lacquer_id','lacquer_status','order_date','delivery_date','quantity','note','measurement_unit_id','delivery_status'];
    public static $insertRoles = [
        'product_img'   => 'nullable',
        'category_id'   => 'required|numeric',
        'model_id'       => 'required|numeric',
        'measurement_unit_id' => 'required|numeric',
        'plainer_id'    => 'nullable|numeric',
        'lacquer_id'    => 'nullable|numeric',
        'order_date'    => 'date|nullable',
        'delivery_date' => 'date|nullable',
        'quantity'      => 'numeric|nullable',
        'note'          => 'nullable',
    ];
    public static $updateRoles = [
        'product_img'   => 'nullable',
        'category_id'   => 'required|numeric',
        'model_id'       => 'required|numeric',
        'measurement_unit_id' => 'required|numeric',
        'plainer_id'    => 'nullable|numeric',
        'lacquer_id'    => 'nullable|numeric',
        'order_date'    => 'date|nullable',
        'delivery_date' => 'date|nullable',
        'quantity'      => 'numeric|nullable',
        'note'          => 'nullable',
    ];

// Product relations with other models starts here
    public function type()
    {
        return $this->belongsTo('App\ProductType','type_id');
    }

    public function model()
    {
        return $this->belongsTo('App\ProductModel','model_id');
    }

    public function category()
    {
        return $this->belongsTo('App\ProductCategory','category_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\MeasurementUnit','measurement_unit_id');
    }

    public function plainer()
    {
        return $this->belongsTo('App\Contractor','plainer_id');
    }

    public function lacquer()
    {
        return $this->belongsTo('App\Contractor','lacquer_id');
    }


    public function assigned_materials()
    {
        return $this->belongsToMany('App\MaterialStock');
    }

    public function materials()
    {
        return $this->belongsToMany('App\Material', 'material_product')->withPivot(['quantity','contractor_id'])->withTimestamps();
    }
// Product relations with other models ends here

}
