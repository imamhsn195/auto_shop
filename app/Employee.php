<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    // Employee Belongs To Department
    
    public function department()
    {
        return $this->belongsTo('App\Department');
    }
    
    public function salaries()
    {
        return $this->hasMany('App\Salary');
    }
   
    public function attendances()
    {
        return $this->hasMany('App\Attendance');
    }
    
    
    public function presents()
    {
        return $this->hasMany('App\Attendance')->where('attendance','p')->count();
    }
    
    public function absents()
    {
        return $this->hasMany('App\Attendance')->where('attendance','a')->count();
    }
     
    public function holydays()
    {
        return $this->hasMany('App\Attendance')->where('attendance','h')->count();
    }
    
}
