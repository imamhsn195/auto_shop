<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    // Supplier Belongs To User
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
