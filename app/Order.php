<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['title','customer_id','vehicle_id','delivery_date', 'note'];

    //A job belongs to a client
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
    //A job belongs to a vehicle
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }
    // A job has many jobdetails
    public function work_list()
    {
       return $this->hasMany('App\OrderWorkDetail');
    }
    // A job has many jobdetails
    public function part_list()
    {
       return $this->hasMany('App\OrderPartDetail');
    }
}
