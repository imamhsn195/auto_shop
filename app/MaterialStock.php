<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialStock extends Model
{
    protected $fillable = ['material_id','quantity','particular','product_id','in_out','contractor_id'];

    public function product()
    {   
        return $this->belongsTo('App\Product');
    }

    public function material()
    {
        return $this->belongsTo('App\Material');
    }

}
