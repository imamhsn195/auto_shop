<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = ['name','description','amount','category_id'];

    public static $insertRoles = [
        'name'=>'required',
        'description' => 'nullable',
        'amount' => 'nullable',
        'category_id' => 'nullable'
    ];
    public static $updateRoles = [
        'name'=>'required',
        'description' => 'nullable',
        'amount' => 'nullable',
        'category_id' => 'nullable'
    ];

    public function category()
    {
        return $this->belongsTo('App\ExpenseCategory');
    }

}
