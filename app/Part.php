<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    
    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }
    
    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
    
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }
    
}
