<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySetting extends Model
{
    protected $fillable = ['name','email','phone','address','logo'];

    public static $insertRoles = [
    'name'=>'required',
    'email' => 'email|nullable',
    'phone' => 'nullable',
    'address' => 'nullable',
    'logo' => 'nullable'
];
    public static $updateRoles = [
        'name'=>'required',
        'email' => 'email|nullable',
        'phone' => 'nullable',
        'address' => 'nullable',
        'logo' => 'nullable'
    ];
}
