<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contractor extends Model
{
    protected $fillable = ['name','email','phone','avatar','address','type_id','salary_type'];

    public static $insertRoles = [
        'name'          =>'required',
        'email'         => 'email|nullable',
        'avatar'        => 'nullable',
        'type_id'       => 'required',
        'salary_type'   => 'required'
    ];
    public static $updateRoles = [
        'name'          =>'required',
        'email'         => 'email|nullable',
        'avatar'        => 'nullable',
        'type_id'       => 'required',
        'salary_type'   => 'required'
    ];


    public function scopePlainers($query)
    {
        return $query->where('type_id',1);
    }
    public function scopeLacquers($query)
    {
        return $query->where('type_id',2);
    }

    public function type()
    {
        return $this->belongsTo('App\ContractorType','type_id');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }
    public function plainer_products(){
        return $this->hasMany('App\Product','plainer_id');
    }
    public function lacquer_products(){
        return $this->hasMany('App\Product','lacquer_id');
    }
}
