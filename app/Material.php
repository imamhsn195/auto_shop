<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $fillable = ['name','type_id','description','unit'];

    public static $insertRoles = [
        'name'=>'required|unique:materials',
        'type_id' => 'required',
        'description' => 'nullable',
    ];
    public static $updateRoles = [
        'name'=>'required',
        'type_id' => 'required',
        'description' => 'nullable'
    ];

    public function type()
    {
        return $this->belongsTo('App\MaterialType');
    }

    
    public function stocks()
    {
        return $this->hasMany('App\MaterialStock');
    }
    
}

