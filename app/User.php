<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'address', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // User has many customers
    public function customers()
    {
        return $this->hasMany('App\Customer', 'user_id');
    }

    //User has many suppliers
    public function suppliers()
    {
        return $this->hasMany('App\Supplier', 'user_id');
    }

    //User has many Employees
    public function employees()
    {
        return $this->hasMany('App\Employee', 'user_id');
    }
}
