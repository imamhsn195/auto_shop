<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    // Customer belongs To User
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function vehicles()
    {
        return $this->hasMany('App\Vehicle', 'customer_id');
    }
}
