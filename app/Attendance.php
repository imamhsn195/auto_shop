<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = ['date','employee_id','attendance','month','year'];
    
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
