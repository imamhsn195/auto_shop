<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable =[
        'name','no','customer_id','driver_name','driver_phone','color'
    ];
    
    public function owner()
    {
    	return $this->belongsTo('App\Customer','customer_id');
    }
}
