<?php

namespace App\Http\Controllers;

use App\Contractor;
use App\MaterialStock;
use App\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function index(){
        /* Total board calculation */
        $total_bords_purchased = MaterialStock::whereHas('material',function ($material){ $material->where('type_id',1); })->where('in_out',0)->get()->sum('quantity');
        $total_bords_used = MaterialStock::whereHas('material',function ($material){ $material->where('type_id',1); })->where('in_out',1)->get()->sum('quantity');
        $total_bords_left = $total_bords_purchased-$total_bords_used;
        /* Total Material Calculation */
        $total_materials_purchased = MaterialStock::whereHas('material',function ($material){ $material->where('type_id',2); })->where('in_out',0)->get()->sum('quantity');
        $total_materials_used = MaterialStock::whereHas('material',function ($material){ $material->where('type_id',2); })->where('in_out',1)->get()->sum('quantity');
        $total_materials_left = $total_materials_purchased-$total_materials_used;
        /* Total plainer products calculation */
        $total_plainer_products = Product::where('plainer_status',0)->get()->count();
        /* Total plainer products calculation */
        $total_lacquer_products = Product::where('lacquer_status',0)->get()->count();
        return view('dashboard.index',compact('total_bords_left','total_materials_left','total_plainer_products','total_lacquer_products'));
    }
}
