<?php

namespace App\Http\Controllers;

use App\MaterialStock;
use Illuminate\Http\Request;

class MaterialStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = $this->validate($request,[
            'material_id' => 'exists:materials,id',
            'quantity' => 'required|numeric',
            'particular' => 'required'
        ]);
        $new_stock = MaterialStock::query()->create($request->all());
        if ($new_stock->id){
            session()->flash('success','Material histroy updated sccessfully');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MaterialStock  $materialStock
     * @return \Illuminate\Http\Response
     */
    public function show(MaterialStock $materialStock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MaterialStock  $materialStock
     * @return \Illuminate\Http\Response
     */
    public function edit(MaterialStock $materialStock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MaterialStock  $materialStock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaterialStock $materialStock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MaterialStock  $materialStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialStock $materialStock)
    {
        //
    }
}
