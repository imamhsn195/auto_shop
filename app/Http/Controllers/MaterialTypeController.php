<?php

namespace App\Http\Controllers;

use App\MaterialType;
use Illuminate\Http\Request;

class MaterialTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $material_types = MaterialType::latest()->paginate(getPaginationItems());
        request('update_material_type_id') ? $update_material_type_info  = $material_types->find(request('update_material_type_id')) : $update_material_type_info  = null;
        return view('materials.material_types',compact('material_types','update_material_type_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(MaterialType::$insertRoles);
        $new_material_type = MaterialType::query()->create($request->all());
        if ($new_material_type->id){
            session()->flash('success','Material Type created sccessfully');
            return redirect()->route('material_types.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MaterialType  $materialType
     * @return \Illuminate\Http\Response
     */
    public function show(MaterialType $materialType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MaterialType  $materialType
     * @return \Illuminate\Http\Response
     */
    public function edit(MaterialType $materialType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MaterialType  $materialType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaterialType $materialType)
    {
        $request->validate(MaterialType::$updateRoles);
        $materialType->update($request->all());
        if ($materialType->id){
            session()->flash('success','Material Type update sccessfully');
            return redirect()->route('material_types.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MaterialType  $materialType
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialType $materialType)
    {
        $materialType->delete();
        session()->flash('success','Material Type deleted sccessfully');
        return redirect()->route('material_types.index');
    }
}
