<?php

namespace App\Http\Controllers;

use App\ProductModel;
use Illuminate\Http\Request;

class ProductModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_models = ProductModel::latest()->paginate(getPaginationItems());
        request('update_product_model_info') !== null ? $update_product_model_info = $product_models->find(request('update_product_model_info')) : $update_product_model_info = null;
        return view('products.product_models',compact('product_models','update_product_model_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(ProductModel::$insertRoles);
        $new_product_model = ProductModel::query()->create($request->all());
        if ($new_product_model->id){
            session()->flash('success','product created sccessfully');
            return redirect()->route('product_models.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function show(ProductModel $productModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductModel $productModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductModel $productModel)
    {
        $request->validate(ProductModel::$updateRoles);
        $productModel->update($request->all());
        if ($productModel->id){
            session()->flash('success','product created sccessfully');
            return redirect()->route('product_models.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductModel $productModel)
    {
        $productModel->delete();
        session()->flash('success','product deleted sccessfully');
        return redirect()->route('product_models.index');
    }
}
