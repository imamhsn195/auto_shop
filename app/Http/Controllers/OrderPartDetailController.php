<?php

namespace App\Http\Controllers;

use App\OrderPartDetail;
use Illuminate\Http\Request;

class OrderPartDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderPartDetail  $orderPartDetail
     * @return \Illuminate\Http\Response
     */
    public function show(OrderPartDetail $orderPartDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderPartDetail  $orderPartDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderPartDetail $orderPartDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderPartDetail  $orderPartDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderPartDetail $orderPartDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderPartDetail  $orderPartDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderPartDetail $orderPartDetail)
    {
        //
    }
}
