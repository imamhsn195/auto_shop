<?php

namespace App\Http\Controllers;

use App\Product;
use App\Material;
use App\Contractor;
use App\MaterialStock;
use App\MaterialProduct;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssignMaterialController extends Controller
{
    public function index()
    {
        $product_name = request('product_name');
        $category = request('category');
        $categories = ProductCategory::latest()->get();
        $products = Product::with('materials')
        ->where(function ($query) use($product_name, $category){
            if ($product_name !== null){
                $query->where('name','like', "%$product_name%");
            }
            if ($category !== null){
                $query->where('category_id',$category);
            }
        })
        ->latest()->get();
        return view('materials.material_assignment_history',compact('products','categories'));
    }

    public function create()
    {
        $products = Product::latest()->where('delivery_status',0)->get();
        $materials = Material::latest()->get();
        return view('materials.assign_material',compact('products','materials'));
    }

    public function assignMaterialToProduct(Request $request)
    {
        $this->validate($request,[
            'product_id' => 'required',
            'material_id.*' => 'nullable'
        ]);
        $product = Product::find($request->product_id);
        $materials = $request->material_id;
        $quantity = $request->quantity;
        foreach($materials as $key => $material){
            $material_type = Material::find($material)->type->id;

            $material_type ? $contractor_id = $product->plainer_id : $product->lacquer_id;

            $search_previous_material_stock = MaterialStock::where([['product_id', $product->id],['material_id',$material]])->first();
            if($search_previous_material_stock == null){
                if($quantity[$key] > 0){

                    $new_record = new MaterialStock();
                    $new_record->product_id = $product->id;
                    $new_record->material_id = $materials[$key];
                    $new_record->in_out = 1;
                    $new_record->particular = 'given to '. $product->name;
                    $new_record->quantity = $quantity[$key];
                    $new_record->save();
                }
            }else{

                // Check if Material Quantity given or not
                    $search_previous_material_stock->quantity = $quantity[$key];
                    $search_previous_material_stock->particular = 'given to '. $product->name;
                    $search_previous_material_stock->update();
            }

            $search_previous_material_product = MaterialProduct::where([['product_id', $product->id],['material_id',$material]])->first();

            if($search_previous_material_product == null){
                if($quantity[$key] > 0){

                    $new_record = new MaterialProduct();
                    $new_record->product_id = $product->id;
                    $new_record->material_id = $materials[$key];
                    $new_record->quantity = $quantity[$key];
                    $new_record->contractor_id = $contractor_id;
                    $new_record->save();
                }
            }else{

                    $search_previous_material_product->quantity = $quantity[$key];
                    $search_previous_material_product->contractor_id = $contractor_id;
                    $search_previous_material_product->update();
            }



        }
        session()->flash('success', 'Material Assigned successfully');
        return redirect()->action('AssignMaterialController@index', ['product_name' => $product->name]);
    }
}
