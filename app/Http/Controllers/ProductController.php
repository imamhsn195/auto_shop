<?php

namespace App\Http\Controllers;

use App\Product;
use App\Material;
use Carbon\Carbon;
use App\Contractor;
use App\ProductType;
use App\ProductModel;
use App\MaterialProduct;
use App\MeasurementUnit;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_name = request('name');
        $category = request('category');
        $plainers = Contractor::plainers()->latest()->get();
        $lacquers = Contractor::lacquers()->latest()->get();
        $product_models = ProductModel::latest()->get();
        $measurement_units = MeasurementUnit::latest()->get();

        $products = Product::latest()
            ->where(function ($query) use($product_name, $category){
                if ($product_name !== null){
                    $query->where('name','like', "%$product_name%");
                }
                if ($category !== null){
                    $query->where('category_id',$category);
                }
            })
       ->where('delivery_status',0)
       ->get();
    //    ->paginate(getPaginationItems());

        request('update_product_id') !== null ? $update_product_info = $products->find(request('update_product_id')):  $update_product_info = null;
        $categories = ProductCategory::latest()->get();
        return view('products.index', compact('products','categories','measurement_units','product_models','update_product_info','plainers','lacquers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plainers = Contractor::plainers()->latest()->get();
        $lacquers = Contractor::lacquers()->latest()->get();
        $product_models = ProductModel::latest()->get();
        $measurement_units = MeasurementUnit::latest()->get();
        $categories = ProductCategory::latest()->get();
        $materials = Material::latest()->get();
        return view('products.create', compact('plainers','lacquers','product_models','measurement_units','categories','materials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(Product::$insertRoles);
        $quantities = request('total_products');
        $new_product = 0;
        for($i = 1; $i <= $quantities; $i++){
            $new_product = Product::query()->create($request->all());

            $product_name = "sl" . $new_product->id."--".$new_product->model->name . "--" .$new_product->category->name . "--" . $new_product->unit->name;
            if($product_name){
                $new_product->fill(['name' => $product_name]);
                $new_product->save();
            }

            $image_name = image_upload($request,'product_img','products');
            if($image_name){
                $new_product->fill(['product_img'=> $image_name]);
                $new_product->save();
            }
        }


        if ($new_product->id){
            session()->flash('success','product created sccessfully');
            return redirect()->route('products.index');
        }else{
            session()->flash('error','product could not be created successfully!');
            return redirect()->route('contractors.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $materials = Material::all();
        foreach($materials as $material){
            $material->quantity = MaterialProduct::where([['product_id',$product->id],['material_id',$material->id]])->first()->quantity ?? 0;
        }
        $sl_counter = 1;
        $materialRows ="
        <div class='table-responsive col-md-6 border-right'>
        <table class='table table-sm table-striped' >
        <thead>
            <tr>
                <th>SL</th>
                <th>Chemical Name</th>
                <th>Chemical In Stock</th>
                <th>Quantity</th>
            </tr>
        </thead>
        <tbody>
        ";
        foreach($materials as $material){
            if($material->type_id == 2){
            $max_input_value = $material->stocks->where('in_out',0)->sum('quantity') -  $material->stocks->where('in_out',1)->sum('quantity') + $material->quantity;
            $materialRows .= "<tr>";
            $materialRows .= "<td>". $sl_counter++ ."</td>";
            $materialRows .= "<td>" . $material->name ."</td>";
            $materialRows .= "<input type='hidden' name ='material_id[]' value='".$material->id."' /></td>";;
            $materialRows .= "<td>". ($material->stocks->where('in_out',0)->sum('quantity') -  $material->stocks->where('in_out',1)->sum('quantity')) . ' '. $material->unit ."</td>";
            $materialRows .= "<td style='width: 15%'><input class='form-control' type='number' name='quantity[]' min='0' max='".$max_input_value."' value='".$material->quantity."' required/></td>";
            $materialRows .= "</tr>";
            }
        }
            $materialRows .= "</tbody></table></div>";
        $materialRows .="
        <div class='table-responsive col-md-6 border-right'>
        <table class='table table-sm table-striped' >
        <thead>
            <tr>
                <th>SL</th>
                <th>Board Name</th>
                <th>Board In Stock</th>
                <th>Quantity</th>
            </tr>
        </thead>
        <tbody>
        ";
        foreach($materials as $material){
                if($material->type_id == 1){
                $max_input_value = $material->stocks->where('in_out',0)->sum('quantity') -  $material->stocks->where('in_out',1)->sum('quantity') + $material->quantity;
                $materialRows .= "<tr>";
                $materialRows .= "<td>". $sl_counter++ ."</td>";
                $materialRows .= "<td>" . $material->name ."</td>";
                $materialRows .= "<input type='hidden' name ='material_id[]' value='".$material->id."' /></td>";
                $materialRows .= "<td>". ($material->stocks->where('in_out',0)->sum('quantity') -  $material->stocks->where('in_out',1)->sum('quantity')) . ' '. $material->unit ."</td>";
                $materialRows .= "<td style='width: 15%'><input class='form-control' type='number' name='quantity[]' min='0' step='.01' max='".$max_input_value."' value='".$material->quantity."' required/></td>";
                $materialRows .= "</tr>";
                }
            }
            $materialRows .= "<tr><td colspan='5'><button type='submit' class='btn btn-primary float-right'>Assign To " . $product->name ."</button></td></tr>";
        $materialRows .= "</tbody></table></div>";
        

            $product = Product::where('id',$product->id)->with('type','unit','category','plainer','lacquer','model')->first();
            $product->materials = $materialRows;
            return response($product,'200');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if (request('delivery_status') !== null) {
            if(request('delivery_status')  == 1){
                $product->delivery_status = 0;
                $product->delivery_date = date("Y-m-d H:i:s");
                $product->update();
                session()->flash('success', 'product delivery status updated sccessfully');
                return back();
            }else{
                $product->delivery_status = 1;
                $product->delivery_date = date("Y-m-d H:i:s");
                $product->update();
                session()->flash('success', 'product delivery status updated sccessfully');
                return back();
            }
        }
        $plainers = Contractor::plainers()->latest()->get();
        $lacquers = Contractor::lacquers()->latest()->get();
        $product_models = ProductModel::latest()->get();
        $measurement_units = MeasurementUnit::latest()->get();
        $categories = ProductCategory::latest()->get();
        $materials = Material::latest()->get();
        return view('products.edit', compact('plainers','lacquers','product_models','measurement_units','categories','materials','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        /* Plainer Status Update*/
        if($request->plainer_status !== null) {
            if ($request->plainer_status == 1) {
                $product->plainer_status = 0;
                $product->update();
                session()->flash('success', 'product plainer status updated sccessfully');
                return back();
            } else {
                $product->plainer_status = 1;
                $product->update();
                session()->flash('success', 'product plainer status updated sccessfully');
                return back();
            }
        }
        /* Lacquer Status update*/
        if ($request->lacquer_status !== null) {
            if($request->lacquer_status == 1){
            $product->lacquer_status = 0;
            $product->update();
            session()->flash('success', 'product lacquer status updated sccessfully');
            return back();
        }else{
                $product->lacquer_status = 1;
                $product->update();
                session()->flash('success', 'product lacquer status updated sccessfully');
                return back();
            }
        }
        /* Product information update */
        $request->validate(Product::$updateRoles);
        if($request->file('product_img')){
            image_delete($product->product_img);
        }
        $product->update($request->all());

        $product_name = "sl" . $product->id."--".$product->model->name . "--" .$product->category->name . "--" . $product->unit->name;
        if($product_name){
            $product->fill(['name' => $product_name]);
            $product->save();
        }

        $image_name = image_upload($request,'product_img','products');
        if($image_name){
            $product->fill(['product_img',$image_name]);
            $product->save();
        }
        if ($product->id){
            session()->flash('success','product updated sccessfully');
            return redirect()->route('products.index');
        }else{
            session()->flash('error','Product could not be created successfully!');
            return redirect()->route('products.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // deleting product disabled
        // if($product->product_img !== 'placeholders/product.png'){
        //     image_delete($product->product_img);
        // }
        // $product->delete();
        // session()->flash('success','product deleted sccessfully');
        return redirect()->route('products.index');
    }
}
