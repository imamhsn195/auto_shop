<?php

namespace App\Http\Controllers;

use App\Contractor;
use App\ContractorType;
use Illuminate\Http\Request;

class ContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contractor_types = ContractorType::latest()->get();
        $contractors = Contractor::latest()->paginate(getPaginationItems());
        request('update_contractor_id') !== null ? $update_contractor_info = $contractors->find(request('update_contractor_id')) : $update_contractor_info = null;
        return view('contractors.index',compact('contractors','update_contractor_info','contractor_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(Contractor::$insertRoles);
        $new_contractor =  Contractor::query()->create($request->all());
        $image_name = image_upload($request,'avatar','contractors');
        if($image_name){
            $new_contractor->fill(['avatar'=> $image_name]);
            $new_contractor->save();
        }
        if($new_contractor->id){
             session()->flash('success','Contractor created successfully!');
             return redirect()->route('contractors.index');
         }else{
             session()->flash('error','Contractor could not be created successfully!');
             return redirect()->route('contractors.index');
         }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function show(Contractor $contractor)
    {
        return response($contractor,'200');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function edit(Contractor $contractor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contractor $contractor)
    {
        $request->validate(Contractor::$updateRoles);
        if($request->file('avatar')){
            image_delete($contractor->avatar);
        }
        $contractor->update($request->all());
        $image_name = image_upload($request,'avatar','contractors');
        if($image_name){
            $contractor->fill(['avatar'=> $image_name]);
            $contractor->save();
        }
        if($contractor->id){
            session()->flash('success','Contractor created successfully!');
            return redirect()->route('contractors.index');
        }else{
            session()->flash('error','Contractor could not be created successfully!');
            return redirect()->route('contractors.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contractor $contractor)
    {
        if($contractor->avatar !== 'placeholders/avatar.png'){
            image_delete($contractor->avatar) ;
        }
        $contractor->delete();
        session()->flash('success','Contractor deleted successfully!');
        return redirect()->route('contractors.index');
    }
}
