<?php

namespace App\Http\Controllers;
use App\User;
use App\Order;
use App\Vehicle;
use App\Customer;
use App\WorkList;
use App\Department;
use App\OrderDetail;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('orders.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        $vehicles = Vehicle::all();
        return view('orders.create',compact('customers','vehicles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'customer_id'=>'required',
            'vehicle_id'=>'required'
        ]);
            $order = new Order;
            $order->customer_id = $request->customer_id;
            $order->vehicle_id = $request->vehicle_id;
            $order->note = $request->customer_note;
            $order->order_no = "Order001";
            $order->delivery = $request->delivery_date;
            $order->save();
            session()->flash('success','Order Added Successfully.');
        return redirect()->route('orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $order = Order::with('owner','vehicle','orderdetails.workLists')->where('id','=',$job_id)->get();
        $orderdetails = OrderDetail::with('workLists.job','workLists.department')->where('job_id','=',$job_id)->get();
        $orderdetails = OrderDetail::with('workLists.job','workLists.department')->where('job_id','=',$job_id)->get();
        $worklist = Department::with('workingLists')->get();


        $deps = Department::all()->KeyBy('name');
        foreach($deps as $d ){
            $deps = OrderDetail::with('workLists.department')->where('job_id','=',$job_id)
            ->wherehas('workLists.department',function($q)use($d){
                $q->where('name','=',$d->name);
            })
            ->get();
                if(count($deps) > 0){
                    $depJobs[$d->name] =$deps;
                }
           }
        return $depJobs;
        return view('orders.edit',compact('job','worklist','jobDetails','$departments','depJobs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    // public function edit(Order $order)
    public function edit()
    {
        return view('orders.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
