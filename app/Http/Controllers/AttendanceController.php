<?php

namespace App\Http\Controllers;

use App\Employee;
use Carbon\Carbon;
use App\Attendance;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = request('date') ?? Carbon::today();
        $attendances = Attendance::where('date',$date)->get();
        return view('attendances.index',compact('attendances','date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all();
        return view('attendances.create',compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach($request->attendances as $employee_id => $attendance){
            Attendance::UpdateOrcreate([
                'date' => $request->date,
                'month' => (string) (datetoread($request->date,'m')-1),
                'year' =>  datetoread($request->date,'y'),
                'employee_id' => $employee_id,
                'attendance' => $attendance,
            ],
            [
                'date' => $request->date,
                'employee_id' => $employee_id,
            ]
            
        );
        }
        session()->flash('success', "Attendance taken for $request->date");
        return redirect()->route('attendances.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        //
    }
}
