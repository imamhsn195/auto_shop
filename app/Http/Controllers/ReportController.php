<?php

namespace App\Http\Controllers;

use App\Product;
use App\Material;
use Carbon\Carbon;
use App\Contractor;
use App\ProductModel;
use App\MaterialStock;
use App\MaterialProduct;
use App\MeasurementUnit;
use App\ProductCategory;
use App\Http\Controllers\Controller;


class ReportController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Material(){
        $materials = Material::all();
        $beginning_date = MaterialStock::all()->first()->updated_at;
        $material_id = request('material') ?? null;
        $start = request('start');
        $end = Carbon::create(request('end'))->addDays(1);

        $stocks = MaterialStock::where([['material_id',$material_id],['quantity' , '>' , 0]])->whereBetween('created_at',[$start,$end])->oldest('updated_at')->get();

        $beginning_stock = MaterialStock::where([['material_id',$material_id],['quantity' , '>' , 0]])->whereBetween('created_at',[$beginning_date,$start])->get();

        $beginning_balance = $beginning_stock->where('in_out',0)->sum('quantity') -  $beginning_stock->where('in_out',1)->sum('quantity');
        return view('reports.material_stock', compact('materials','stocks','beginning_balance'));
    }

    function Contractor(){
        $contractors = Contractor::all();
        $contractor_id  = request('contractor');
        $product_name  = request('product_name');
        $contractor_type = $contractors->find($contractor_id)->type_id ?? null;
        // $start = request('start'); // datewise search disabled
        // $end = Carbon::create(request('end'))->addDays(1); // datewise search disabled
        // $products = Product::whereBetween('created_at',[$start,$end]) // Datewise search disabled
        $products = Product::where('delivery_status' , 0)
            ->where(function ($query) use($contractor_id , $contractor_type, $product_name){
                if($contractor_type == 1){
                    $query->where([['plainer_id',$contractor_id],['plainer_status',1]]);
                }
                if($contractor_type == 2){
                    $query->where([['lacquer_id',$contractor_id],['lacquer_status',1]]);
                }
                if($product_name !== null){
                    $query->where('name','like',"%$product_name%");
                }
            })
            ->latest()
            ->get();
        return view('reports.contractor_report', compact('products','contractors'));
    }

    public function delivered_products()
    {
        $product_name = request('name');
        $category = request('category');
        $plainers = Contractor::plainers()->latest()->get();
        $lacquers = Contractor::lacquers()->latest()->get();
        $product_models = ProductModel::latest()->get();
        $measurement_units = MeasurementUnit::latest()->get();

        $products = Product::where(function ($query) use($product_name, $category){
                if ($product_name !== null){
                    $query->where('name','like', "%$product_name%");
                }
                if ($category !== null){
                    $query->where('category_id',$category);
                }
            })
       ->where('delivery_status',1)
       ->latest('delivery_date')
       ->paginate(getPaginationItems());

        request('update_product_id') !== null ? $update_product_info = $products->find(request('update_product_id')):  $update_product_info = null;
        $categories = ProductCategory::latest()->get();
        return view('reports.delivered_products', compact('products','categories','measurement_units','product_models','update_product_info','plainers','lacquers'));
    }
}
