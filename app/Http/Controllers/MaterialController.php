<?php

namespace App\Http\Controllers;

use App\Material;
use App\Contractor;
use App\MaterialType;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = MaterialType::latest()->get();
        $material_name = request('name');
        $material_type = request('type');
        $contractors = Contractor::all();
        $materials = Material::latest()
            ->where(function ($query) use($material_name, $material_type){
                if ($material_name !== null){
                    $query->where('name','like',"%$material_name%");
                }
                if ($material_type !== null){
                    $query->where('type_id',$material_type);
                }
            })
            ->get();
            // ->paginate(getPaginationItems());
        request('update_material_id') ? $update_material_info = $materials->find(request('update_material_id')) : $update_material_info = null;
        return view('materials.index',compact('materials','update_material_info','types','contractors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->type_id == 1 ? $request['unit'] = 'pics' : $request['unit'] = 'grams';
        $request->validate(Material::$insertRoles);
        $new_material = Material::query()->create($request->all());
        if ($new_material->id){
            session()->flash('success','Material created sccessfully');
            return redirect()->route('materials.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function show(Material $material)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function edit(Material $material)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Material $material)
    {
        $request->type_id == 1 ? $request['unit'] = 'pics' : $request['unit'] = 'grams';
        $request->validate(Material::$updateRoles);
        $material->update($request->all());
        if ($material->id){
            session()->flash('success','Material update sccessfully');
            return redirect()->route('materials.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Material $material)
    {
        $material->delete();
        session()->flash('success','Material deleted sccessfully');
        return redirect()->route('materials.index');
    }
}
