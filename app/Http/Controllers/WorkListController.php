<?php

namespace App\Http\Controllers;

use App\WorkList;
use App\Department;
use Illuminate\Http\Request;

class WorkListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        $work_lists = WorkList::where(function($worklist){
            if(request('name')){
                $worklist->where('name', 'like', "%".request('name')."%");
            }
            if(request('department_id')){
                $worklist->where('department_id', request('department_id'));
            }
        })->get();
        return view('worklists.index',compact('work_lists','departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('worklists.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        WorkList::create(request()->validate([
            'name'=>'required',
            'department_id'=>'required',
            'rate1'=>'numeric',
            'rate2'=>'numeric',
            'rate3'=>'numeric'
        ])); 
        return back()->with('success','Worklist Added Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkList  $workList
     * @return \Illuminate\Http\Response
     */
    public function show(WorkList $workList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkList  $workList
     * @return \Illuminate\Http\Response
     */
   public function edit(WorkList $workList)
    {
        $departments = Department::all();
        return view('worklists.edit', compact('workList','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkList  $workList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkList $workList) //$id need to be solved
    {

       $this->validate($request,[
            'name'=>'required',
            'department_id'=>'required',
            'rate1'=>'numeric',
            'rate2'=>'numeric',
            'rate3'=>'numeric'
        ]); 

        $workList->name = $request->name;
        $workList->department_id = $request->department_id;
        $workList->rate1= $request->rate1;
        $workList->rate2= $request->rate2;
        $workList->rate3= $request->rate3;
        $workList->update();
        session()->flash('success','Worklist Added Successfully.');
        return redirect()->route('work_lists.index');
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkList  $workList
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkList $workList) //$id Need to be solved or removed
    {
        $workingList = WorkList::find($id);
        $workingList->delete();
        return back()->with('success','Work list deleted successfully!');
    }
}
