<?php

namespace App\Http\Controllers;

use App\User;
use App\Employee;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeController extends Controller
{
    use SoftDeletes;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::orderBy('department_id')->where(function($employee){
            if(request('name')){
                 $employee->whereHas('user', function($user){
                    $user->where('name',request('name'));
                 });
            }
            if(request('department')){
                $employee->whereHas('department', function($department){
                    $department->where('id',request('department'));
                 });
            }
           
        })->get();
        $departments = Department::all();
        return view('employees.index',compact('employees','departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all();
        $departments = Department::all();
        return view('employees.create', compact('employees','departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // retu\rn $request->all();
        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required'
        ]);

        $employee = new Employee();
        $employee->salary = $request->salary;
        $employee->department_id = $request->department_id;
        $employee->name = $request->name;
        $employee->phone = $request->phone;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->password = Hash::make($request->password);
        $employee->joining_date = $request->joining_date;
        $employee->save();
        session()->flash('success','Employee Registration Successful');
        return redirect()->route('employees.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('employees.show',compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $departments = Department::all();
        return view('employees.edit', compact('employee','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        // return $request->all();
        $this->validate($request,[
            'name' => 'required',
            // 'phone' => 'unique:users'
        ]);

        $employee->salary = $request->salary;
        $employee->department_id = $request->department_id;
        $employee->name = $request->name;
        $employee->phone = $request->phone;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->password = Hash::make($request->password);
        $employee->joining_date = $request->joining_date;
        $employee->update();
        session()->flash('success','Employee Registration Successful');
        return redirect()->route('employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        session()->flash('success','Employee deleted successfully');
        return redirect()->route('employees.index');
    }
}
