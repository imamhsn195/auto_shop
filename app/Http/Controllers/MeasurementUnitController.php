<?php

namespace App\Http\Controllers;

use App\MeasurementUnit;
use Illuminate\Http\Request;

class MeasurementUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $measurement_units = MeasurementUnit::latest()->paginate(getPaginationItems());
        request('update_measurement_unit_id') ? $update_measurement_unit_info = $measurement_units->find(request('update_measurement_unit_id')) : $update_measurement_unit_info = null;
        return view('measurement_units.index',compact('measurement_units','update_measurement_unit_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(MeasurementUnit::$insertRoles);
        $new_measurement_unit = MeasurementUnit::query()->create($request->all());
        if ($new_measurement_unit->id){
            session()->flash('success','Measurement Unit created sccessfully');
            return redirect()->route('measurement_units.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MeasurementUnit  $measurementUnit
     * @return \Illuminate\Http\Response
     */
    public function show(MeasurementUnit $measurementUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MeasurementUnit  $measurementUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(MeasurementUnit $measurementUnit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MeasurementUnit  $measurementUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MeasurementUnit $measurementUnit)
    {
        $request->validate(MeasurementUnit::$updateRoles);
        $measurementUnit->update($request->all());
        if ($measurementUnit->id){
            session()->flash('success','Measurement Unit update sccessfully');
            return redirect()->route('measurement_units.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MeasurementUnit  $measurementUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy(MeasurementUnit $measurementUnit)
    {
        $measurementUnit->delete();
        session()->flash('success','Measurement Unit deleted sccessfully');
        return redirect()->route('measurement_units.index');
    }
}
