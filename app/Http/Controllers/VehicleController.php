<?php

namespace App\Http\Controllers;

use App\User;
use App\Vehicle;
use App\Customer;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $vehicles = Vehicle::where(function($vehicles){
            if(request('name')){
                $vehicles->where('name','like', "%".request('name')."%")->orWhere('no','like', "%".request('name')."%");
            }
        })->get();  
        return view('vehicles.index',compact('vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        return view('vehicles.create',compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'no'=>'required'
        ]);
        $vahicle = new Vehicle();
        $vahicle->name = $request->name;
        $vahicle->no = $request->no;
        $vahicle->customer_id = $request->customer_id;
        $vahicle->driver_name = $request->driver_name;
        $vahicle->driver_phone = $request->driver_phone;
        $vahicle->color = $request->color;
        $vahicle->save();
        session()->flash('success','Vehicle Added Successfully.');
        return redirect()->route('vehicles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(vehicle $vehicle)
    {
        $customers = Customer::all();
        return view('vehicles.edit', compact('vehicle','customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vehicle $vehicle)
    {
        $this->validate($request,[
            'name'=>'required',
            'no'=>'required'
        ]);
        $vehicle->name = $request->name;
        $vehicle->no = $request->no;
        $vehicle->customer_id = $request->customer_id;
        $vehicle->driver_name = $request->driver_name;
        $vehicle->driver_phone = $request->driver_phone;
        $vehicle->color = $request->color;
        $vehicle->update();
        session()->flash('success','Vehicle updated Successfully.');
        return redirect()->route('vehicles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(vehicle $vehicle)
    {
        //
    }
}
