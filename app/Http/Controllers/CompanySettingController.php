<?php

namespace App\Http\Controllers;

use App\CompanySetting;
use Illuminate\Http\Request;

class CompanySettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_settings = CompanySetting::latest()->first();
        return view('company_settings.index', compact('company_settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanySetting  $companySetting
     * @return \Illuminate\Http\Response
     */
    public function show(CompanySetting $companySetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanySetting  $companySetting
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanySetting $companySetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanySetting  $companySetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanySetting $companySetting)
    {
        $request->validate(CompanySetting::$updateRoles);

        if($request->file('logo')){
            image_delete($companySetting->logo);
        }
        $companySetting->update($request->all());
        
        $image_name = image_upload($request,'logo','logos','logo.png');

        if($image_name){
            $companySetting->fill(['logo'=> $image_name]);
            $companySetting->save();
        }
        
        if($companySetting->id){
            session()->flash('success','Company Settings updated successfully!');
            return redirect()->route('company_settings.index');
        }else{
            session()->flash('error','Company settings could not be updated successfully!');
            return redirect()->route('company_settings.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanySetting  $companySetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanySetting $companySetting)
    {
        //
    }
}
