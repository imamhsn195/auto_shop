<?php

namespace App\Http\Controllers;

use App\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_types = ProductType::latest()->paginate(getPaginationItems());
        request('update_product_type_info') !== null ? $update_product_type_info = $product_types->find(request('update_product_type_info')) : $update_product_type_info = null;
        return view('products.product_types',compact('product_types','update_product_type_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(ProductType::$insertRoles);
        $new_product_type = ProductType::query()->create($request->all());
        if ($new_product_type->id){
            session()->flash('success','product created sccessfully');
            return redirect()->route('product_types.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function show(ProductType $productType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductType $productType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductType $productType)
    {
        $request->validate(ProductType::$updateRoles);
        $productType->update($request->all());
        if ($productType->id){
            session()->flash('success','product created sccessfully');
            return redirect()->route('product_types.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductType $productType)
    {
        $productType->delete();
        session()->flash('success','product deleted sccessfully');
        return redirect()->route('product_types.index');
    }
}
