<?php

namespace App\Http\Controllers;

use App\OrderWorkDetail;
use Illuminate\Http\Request;

class OrderWorkDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderWorkDetail  $orderWorkDetail
     * @return \Illuminate\Http\Response
     */
    public function show(OrderWorkDetail $orderWorkDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderWorkDetail  $orderWorkDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderWorkDetail $orderWorkDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderWorkDetail  $orderWorkDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderWorkDetail $orderWorkDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderWorkDetail  $orderWorkDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderWorkDetail $orderWorkDetail)
    {
        //
    }
}
