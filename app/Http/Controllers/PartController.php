<?php

namespace App\Http\Controllers;

use App\Part;
use App\Unit;
use App\Brand;
use App\Supplier;
use Illuminate\Http\Request;

class PartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parts = Part::all();
        return view('parts.index', compact('parts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::all();
        $units = Unit::all();
        $brands = Brand::all();
        $parts = Part::all();
        return view('parts.create', compact('parts','suppliers','units','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $part = new Part();
        $part->name = $request->name;
        $part->supplier_id = $request->supplier_id;
        $part->unit_id = $request->unit_id;
        $part->brand_id = $request->brand_id;
        $part->purchase_price = $request->purchase_price;
        $part->sale_price = $request->sale_price;
        $part->save();
        session()->flash('success','Part register successfull!');
        return redirect()->route('parts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Part  $part
     * @return \Illuminate\Http\Response
     */
    public function show(Part $part)
    {
        return view('parts.show', compact('part'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Part  $part
     * @return \Illuminate\Http\Response
     */
    public function edit(Part $part)
    {
        
        $suppliers = Supplier::all();
        $units = Unit::all();
        $brands = Brand::all();
        return view('parts.edit', compact('part','suppliers','units','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Part  $part
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Part $part)
    {
        $part->name = $request->name;
        $part->supplier_id = $request->supplier_id;
        $part->unit_id = $request->unit_id;
        $part->brand_id = $request->brand_id;
        $part->purchase_price = $request->purchase_price;
        $part->sale_price = $request->sale_price;
        $part->update();
        session()->flash('success', 'Part update successfull');
        return redirect()->route('parts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Part  $part
     * @return \Illuminate\Http\Response
     */
    public function destroy(Part $part)
    {
        //
    }
}
