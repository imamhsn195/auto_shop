<?php

namespace App\Http\Controllers;

use App\ContractorType;
use Illuminate\Http\Request;

class ContractorTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contractor_types = ContractorType::latest()->paginate(getPaginationItems());
        request('update_contractor_type_info') !== null ? $update_contractor_type_info = $contractor_types->find(request('update_contractor_type_info')) : $update_contractor_type_info = null;
        return view('contractors.contractor_types',compact('contractor_types','update_contractor_type_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(ContractorType::$insertRoles);
        $new_contractor_type = ContractorType::query()->create($request->all());
        if ($new_contractor_type->id){
            session()->flash('success','contactor created sccessfully');
            return redirect()->route('contractor_types.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContractorType  $contractorType
     * @return \Illuminate\Http\Response
     */
    public function show(ContractorType $contractorType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContractorType  $contractorType
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractorType $contractorType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContractorType  $contractorType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContractorType $contractorType)
    {
        $request->validate(ContractorType::$updateRoles);

        $contractorType->update($request->all());
        if ($contractorType->id){
            session()->flash('success','contactor created sccessfully');
            return redirect()->route('contractor_types.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContractorType  $contractorType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractorType $contractorType)
    {
        $ContractorType->delete();
        session()->flash('success','contactor deleted sccessfully');
        return redirect()->route('contractor_types.index');
    }
}
